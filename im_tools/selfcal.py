"""
Performs phase or amplitude and phase self-calibration with
task SELFCAL and re-images after the gain solutions are
applied.
"""

import os
import re
import subprocess
import sys

import numpy

from . import generate_map


def _check_phase(logfile, bins):
    """
    Reads phase RMS after selfcal from file.

    :param logfile: Logger from which the phase RMS is read.
    :param bins: Number of bins over which selfcal solutions
        should be determined.

    :return: RMS of phase(s) after selfcal.
    """
    lines = open(logfile).readlines()
    line = [
        entry
        for entry in lines
        if re.search("SELFCAL: Rms of the gain phases", entry, re.IGNORECASE)
    ]
    if bins == 1:
        rms_phase = float(line[-1].split(":")[2])
    else:
        rms_phase = float(line[-(bins + 1)].split(":")[2])

    return rms_phase


def _check_map_rms(initial_logfile, recent_logfile):
    """
    Reads RMS in CLEAN maps from a logfile.

    :param initial_logfile: Logger from which the RMS of the
        initial map is read.
    :param recent_logfile: Logger from which the RMS of the
        self-calibrated map is read.

    :return: RMS in initial and self-calibrated CLEAN maps.
    """
    initial_map_rms = open(initial_logfile).readlines()[4].split()[-1]
    recent_map_rms = open(recent_logfile).readlines()[4].split()[-1]

    return float(initial_map_rms), float(recent_map_rms)


def _write_tofile(info: str):
    """
    Write failed pointing information to file. Writes information
    about selfcal and RMS of clean map to file for further
    investigation by the user if the phase and/or rms optional
    arguments are set.

    :param info: Information about failed selfcal or selfcal
        failing to improve the RMS in a map.

    :return: None
    """
    new_file = open("%s/failed_peeling.txt" % os.getcwd(), "a+")
    new_file.write("%s\n" % info)
    new_file.close()


def calibrate_and_image(
    input_dir: str,
    model_dir: str,
    off_axis_source_vis: str,
    solint: float,
    uvrange: tuple,
    refant: int,
    bins: int,
    accept_phase: float,
    imsize: int,
    cell: float,
    robust: float,
    isigma: tuple[float, ...],
    pol_sigma: tuple[float, ...],
    niters: int,
    beam: tuple[float, float, float],
    all_stokes: bool = False,
    nov: bool = False,
    ampcal: bool = False,
    check_map_rms: bool = False,
    check_phase: bool = False,
):
    """
    Performs self-calibration and re-images. Optionally checks each round of
    self-calibration improves the CLEAN map. Also, optionally checks the RMS
    of the phases during self-calibration is below a user-defined threshold.

    :param input_dir: Directory containing maps from which the off-axis
        source would be removed.
    :param model_dir: Directory containing model map(s) to use.
    :param off_axis_source_vis: The visibilities containing only the off-axis
        source.
    :param solint: Solution interval (in minutes).
    :param uvrange: UVrange over which selfcal should be performed.
    :param refant: Reference antenna.
    :param bins: Number of bins to determine gains solutions.
    :param accept_phase: RMS of phases beyond which the gain solutions
        should be rejected.
    :param imsize: Image size (in arcsec).
    :param cell: Cell size (in arcsec).
    :param robust: Briggs' robust weighting.
    :param isigma: Sigma level threshold for deconvolving Stokes I maps.
    :param pol_sigma: Sigma level threshold for deconvolving polarised maps.
    :param niters: Maximum number of minor iterations for deconvolution.
    :param beam: Restoring beam size(bmaj(arcsec) bmin(arcsec) pa(degrees).
    :param all_stokes: Perform peeling on all Stokes (I, Q, U, V) maps.
    :param nov: Exclude peeling on Stokes V map.
    :param ampcal: Boolean to perform amplitude selfcal or not.
    :param check_map_rms: Checks each round of self-calibration improves CLEAN
        map.
    :param check_phase: Assert RMS of phases is better than the user-defined
        acceptable value.

    :return: Returns gain solutions and self-calibrated maps.
    """
    # Generate initial maps
    if all_stokes:
        if nov:
            im_params = ["i", "q", "u"]
        else:
            im_params = ["i", "q", "u", "v"]
    else:
        im_params = ["i"]
    for stokes in im_params:
        generate_map.imaging(
            input_dir,
            model_dir,
            off_axis_source_vis,
            stokes,
            imsize,
            cell,
            robust,
            isigma[0],
            pol_sigma[0],
            niters,
            None,
            beam,
        )

    # Self-calibrate and re-image
    pnt = os.path.basename(off_axis_source_vis).split(".")[0]
    for idx, val in enumerate(solint):
        if uvrange is None:
            # ==============================
            # SELFCAL OVER FULL UVRANGE
            # ===============================
            for stokes in im_params:
                if stokes == "i":
                    if not os.path.isdir(
                        "%s/0%s_selfcal/" % (off_axis_source_vis, idx + 1)
                    ):
                        subprocess.run(
                            [
                                "mkdir",
                                "%s/0%s_selfcal/"
                                % (off_axis_source_vis, idx + 1),
                            ],
                            check=True,
                        )
                        subprocess.run(
                            [
                                "cp",
                                "%s/visdata" % off_axis_source_vis,
                                "%s/vartable" % off_axis_source_vis,
                                "%s/history" % off_axis_source_vis,
                                "%s/header" % off_axis_source_vis,
                                "%s/flags" % off_axis_source_vis,
                                "%s/0%s_selfcal"
                                % (off_axis_source_vis, idx + 1),
                            ],
                            check=True,
                        )
                    # Selfcal iff the gain solutions do not exist
                    if not os.path.isfile(
                        "%s/0%s_selfcal/gains" % (off_axis_source_vis, idx + 1)
                    ):
                        print(
                            "\nSELFCAL No. %s begins: %s min(s) ..."
                            % (idx + 1, val)
                        )
                        if idx == 0:
                            sky_model = os.path.join(
                                "%s" % off_axis_source_vis, "_imodel"
                            )
                            pix_val = max(
                                numpy.genfromtxt(
                                    "%s/_mf.ilog" % off_axis_source_vis,
                                    invalid_raise=False,
                                    usecols=3,
                                )
                            )
                        else:
                            sky_model = os.path.join(
                                "%s" % off_axis_source_vis,
                                "0%s_selfcal" % idx,
                                "_imodel",
                            )
                            pix_val = max(
                                numpy.genfromtxt(
                                    "%s/0%s_selfcal/_mf.ilog"
                                    % (off_axis_source_vis, idx),
                                    invalid_raise=False,
                                    usecols=3,
                                )
                            )
                        if pix_val < 0:
                            pix_val = 0
                        if refant is None:
                            if ampcal and idx == len(solint) - 1:
                                subprocess.run(
                                    [
                                        "selfcal",
                                        "vis=%s" % off_axis_source_vis,
                                        "model=%s" % sky_model,
                                        "clip=%s" % pix_val,
                                        "interval=%s"
                                        % (solint[len(solint) - 1]),
                                        "nfbin=%s" % bins,
                                        "options=mfs,amplitude",
                                    ],
                                    check=True,
                                )
                            else:
                                subprocess.run(
                                    [
                                        "selfcal",
                                        "vis=%s" % off_axis_source_vis,
                                        "model=%s" % sky_model,
                                        "clip=%s" % pix_val,
                                        "interval=%s" % val,
                                        "nfbin=%s" % bins,
                                        "options=mfs,phase",
                                    ],
                                    check=True,
                                )
                        else:
                            if ampcal and idx == len(solint) - 1:
                                subprocess.run(
                                    [
                                        "selfcal",
                                        "vis=%s" % off_axis_source_vis,
                                        "model=%s" % sky_model,
                                        "clip=%s" % pix_val,
                                        "interval=%s"
                                        % (solint[len(solint) - 1]),
                                        "refant=%s" % refant,
                                        "nfbin=%s" % bins,
                                        "options=mfs,amplitude",
                                    ],
                                    check=True,
                                )
                            else:
                                subprocess.run(
                                    [
                                        "selfcal",
                                        "vis=%s" % off_axis_source_vis,
                                        "model=%s" % sky_model,
                                        "clip=%s" % pix_val,
                                        "interval=%s" % val,
                                        "refant=%s" % refant,
                                        "nfbin=%s" % bins,
                                        "options=mfs,phase",
                                    ],
                                    check=True,
                                )
                        # Save history and copy gain solutions
                        subprocess.run(
                            [
                                "prthis",
                                "in=%s" % off_axis_source_vis,
                                "log=%s/0%s_selfcal/_prthis.log"
                                % (off_axis_source_vis, idx + 1),
                            ],
                            check=True,
                        )
                        subprocess.run(
                            [
                                "gpcopy",
                                "vis=%s" % off_axis_source_vis,
                                "out=%s/0%s_selfcal"
                                % (off_axis_source_vis, idx + 1),
                            ],
                            check=True,
                        )
                        if bins == 1:
                            subprocess.run(
                                ["rm", "%s/gains" % off_axis_source_vis],
                                check=True,
                            )
                        else:
                            subprocess.run(
                                [
                                    "rm",
                                    "%s/gains" % off_axis_source_vis,
                                    "%s/gainsf" % off_axis_source_vis,
                                ],
                                check=True,
                            )
                # Assert acceptable RMS
                if check_phase:
                    rms_phase = _check_phase(
                        "%s/0%s_selfcal/_prthis.log"
                        % (off_axis_source_vis, idx + 1),
                        bins,
                    )
                    if rms_phase > accept_phase:
                        _write_tofile(
                            "SELFCAL No. %s on %s failed!" % (idx + 1, pnt)
                        )
                        sys.exit("\nRMS of phases exceeded acceptable value!")
                # Re-image self-calibrated data
                if idx == 0:
                    prev_model = os.path.join(
                        "%s" % off_axis_source_vis, "_%smodel" % stokes
                    )
                else:
                    prev_model = os.path.join(
                        "%s" % off_axis_source_vis,
                        "0%s_selfcal" % idx,
                        "_%smodel" % stokes,
                    )
                off_axis_source_vis = os.path.join(
                    "%s" % off_axis_source_vis, "0%s_selfcal" % (idx + 1)
                )
                generate_map.imaging(
                    input_dir,
                    model_dir,
                    off_axis_source_vis,
                    stokes,
                    imsize,
                    cell,
                    robust,
                    isigma[1],
                    pol_sigma[1],
                    niters,
                    prev_model,
                    beam,
                )
                if check_map_rms:
                    # Assert the selfcal improved the map
                    if idx == 0:
                        initial_file = os.path.join(
                            os.path.split(off_axis_source_vis)[0],
                            "_%srms.log" % stokes,
                        )
                    else:
                        initial_file = os.path.join(
                            os.path.split(off_axis_source_vis)[0],
                            "0%s_selfcal" % idx,
                            "_%srms.log" % stokes,
                        )
                    recent_file = "%s/_%srms.log" % (
                        off_axis_source_vis,
                        stokes,
                    )
                    initial_rms, recent_rms = _check_map_rms(
                        initial_file, recent_file
                    )
                    if recent_rms >= initial_rms:
                        _write_tofile(
                            "SELFCAL No. %s failed to improve Stokes %s map!"
                            % (idx + 1, stokes)
                        )
                        sys.exit(
                            "\nSELFCAL No. %s failed to improve Stokes %s map!"
                            % (idx + 1, stokes)
                        )
                # Pass directory containing initial maps for the next selfcal to work
                off_axis_source_vis = os.path.split(off_axis_source_vis)[0]
        else:
            # ==================================
            # SELFCAL OVER SELECTED UVRANGE
            # ===================================
            for stokes in im_params:
                if idx == 0:
                    for j, uv in enumerate(uvrange):
                        if stokes == "i":
                            if not os.path.isdir(
                                "%s/0%s_selfcal/"
                                % (off_axis_source_vis, idx + 1)
                            ):
                                subprocess.run(
                                    [
                                        "mkdir",
                                        "%s/0%s_selfcal/"
                                        % (off_axis_source_vis, idx + 1),
                                    ],
                                    check=True,
                                )
                            if not os.path.isdir(
                                "%s/0%s_selfcal/0%s/"
                                % (off_axis_source_vis, idx + 1, j + 1)
                            ):
                                subprocess.run(
                                    [
                                        "mkdir",
                                        "%s/0%s_selfcal/0%s/"
                                        % (
                                            off_axis_source_vis,
                                            idx + 1,
                                            j + 1,
                                        ),
                                    ],
                                    check=True,
                                )
                                subprocess.run(
                                    [
                                        "cp",
                                        "%s/visdata" % off_axis_source_vis,
                                        "%s/vartable" % off_axis_source_vis,
                                        "%s/history" % off_axis_source_vis,
                                        "%s/header" % off_axis_source_vis,
                                        "%s/flags" % off_axis_source_vis,
                                        "%s/0%s_selfcal/0%s"
                                        % (
                                            off_axis_source_vis,
                                            idx + 1,
                                            j + 1,
                                        ),
                                    ],
                                    check=True,
                                )
                            # Selfcal iff the gain solutions do not exist
                            if not os.path.isfile(
                                "%s/0%s_selfcal/0%s/gains"
                                % (off_axis_source_vis, idx + 1, j + 1)
                            ):
                                print(
                                    "\nSELFCAL No. %s begins: %s min(s) over %s k-lambda ..."
                                    % (idx + 1, val, uv)
                                )
                                if j == 0:
                                    sky_model = os.path.join(
                                        off_axis_source_vis, "_imodel"
                                    )
                                    pix_val = max(
                                        numpy.genfromtxt(
                                            "%s/_mf.ilog"
                                            % off_axis_source_vis,
                                            invalid_raise=False,
                                            usecols=3,
                                        )
                                    )
                                else:
                                    sky_model = os.path.join(
                                        off_axis_source_vis,
                                        "0%s_selfcal" % (idx + 1),
                                        "0%s" % j,
                                        "_imodel",
                                    )
                                    pix_val = max(
                                        numpy.genfromtxt(
                                            "%s/0%s_selfcal/0%s/_mf.ilog"
                                            % (
                                                off_axis_source_vis,
                                                idx + 1,
                                                j,
                                            ),
                                            invalid_raise=False,
                                            usecols=3,
                                        )
                                    )
                                if pix_val < 0:
                                    pix_val = 0
                                if refant is None:
                                    subprocess.run(
                                        [
                                            "selfcal",
                                            "vis=%s" % off_axis_source_vis,
                                            "'select=uvrange(%s)'" % uv,
                                            "model=%s" % sky_model,
                                            "clip=%s" % pix_val,
                                            "interval=%s" % val,
                                            "nfbin=%s" % bins,
                                            "options=mfs,phase",
                                        ],
                                        check=True,
                                    )
                                else:
                                    subprocess.run(
                                        [
                                            "selfcal",
                                            "vis=%s" % off_axis_source_vis,
                                            "'select=uvrange(%s)'" % uv,
                                            "model=%s" % sky_model,
                                            "clip=%s" % pix_val,
                                            "interval=%s" % val,
                                            "refant=%s" % refant,
                                            "nfbin=%s" % bins,
                                            "options=mfs,phase",
                                        ],
                                        check=True,
                                    )
                                subprocess.run(
                                    [
                                        "prthis",
                                        "in=%s" % off_axis_source_vis,
                                        "log=%s/0%s_selfcal/0%s/_prthis.log"
                                        % (
                                            off_axis_source_vis,
                                            idx + 1,
                                            j + 1,
                                        ),
                                    ],
                                    check=True,
                                )
                                subprocess.run(
                                    [
                                        "gpcopy",
                                        "vis=%s" % off_axis_source_vis,
                                        "out=%s/0%s_selfcal/0%s"
                                        % (
                                            off_axis_source_vis,
                                            idx + 1,
                                            j + 1,
                                        ),
                                    ],
                                    check=True,
                                )
                                if bins == 1:
                                    subprocess.run(
                                        [
                                            "rm",
                                            "%s/gains" % off_axis_source_vis,
                                        ],
                                        check=True,
                                    )
                                else:
                                    subprocess.run(
                                        [
                                            "rm",
                                            "%s/gains" % off_axis_source_vis,
                                            "%s/gainsf" % off_axis_source_vis,
                                        ],
                                        check=True,
                                    )
                        if check_phase and j == len(uvrange) - 1:
                            rms_phase = _check_phase(
                                "%s/0%s_selfcal/0%s/_prthis.log"
                                % (off_axis_source_vis, idx + 1, j + 1),
                                bins,
                            )
                            if rms_phase > accept_phase:
                                _write_tofile(
                                    "SELFCAL No. %s over %s k-lambda on %s failed!"
                                    % (idx + 1, uv, pnt)
                                )
                                sys.exit(
                                    "\nRMS of phases exceeded acceptable value!"
                                )
                        # Re-image self-calibrated data
                        if j == 0:
                            prev_model = os.path.join(
                                off_axis_source_vis, "_%smodel" % stokes
                            )
                        else:
                            prev_model = os.path.join(
                                off_axis_source_vis,
                                "0%s_selfcal" % (idx + 1),
                                "0%s" % j,
                                "_%smodel" % stokes,
                            )
                        off_axis_source_vis = os.path.join(
                            off_axis_source_vis,
                            "0%s_selfcal" % (idx + 1),
                            "0%s" % (j + 1),
                        )
                        generate_map.imaging(
                            input_dir,
                            model_dir,
                            off_axis_source_vis,
                            stokes,
                            imsize,
                            cell,
                            robust,
                            isigma[1],
                            pol_sigma[1],
                            niters,
                            prev_model,
                            beam,
                        )
                        if check_map_rms and j == len(uvrange) - 1:
                            # Assert the selfcal improved the map
                            initial_file = os.path.join(
                                os.path.split(
                                    os.path.split(off_axis_source_vis)[0]
                                )[0],
                                "_%srms.log" % stokes,
                            )
                            recent_file = os.path.join(
                                off_axis_source_vis, "_%srms.log" % stokes
                            )
                            initial_map_rms, recent_map_rms = _check_map_rms(
                                initial_file, recent_file
                            )
                            if recent_map_rms >= initial_map_rms:
                                _write_tofile(
                                    "SELFCAL No. %s over %s k-lambda on %s failed to improve Stokes %s map!"
                                    % (
                                        idx + 1,
                                        uvrange[len(uvrange) - 1],
                                        pnt,
                                        stokes.upper(),
                                    )
                                )
                                sys.exit(
                                    "\nSELFCAL failed to improve the Stokes %s map!"
                                    % stokes.upper()
                                )
                        # Pass the directory containing the initial maps for the selfcal to work
                        off_axis_source_vis = os.path.split(
                            os.path.split(off_axis_source_vis)[0]
                        )[0]
                else:
                    if stokes == "i":
                        if not os.path.isdir(
                            "%s/0%s_selfcal/" % (off_axis_source_vis, idx + 1)
                        ):
                            subprocess.run(
                                [
                                    "mkdir",
                                    "%s/0%s_selfcal/"
                                    % (off_axis_source_vis, idx + 1),
                                ],
                                check=True,
                            )
                            subprocess.run(
                                [
                                    "cp",
                                    "%s/visdata" % off_axis_source_vis,
                                    "%s/vartable" % off_axis_source_vis,
                                    "%s/history" % off_axis_source_vis,
                                    "%s/header" % off_axis_source_vis,
                                    "%s/flags" % off_axis_source_vis,
                                    "%s/0%s_selfcal"
                                    % (off_axis_source_vis, idx + 1),
                                ],
                                check=True,
                            )
                        # Selfcal iff the solutions do not exist
                        if not os.path.isfile(
                            "%s/0%s_selfcal/gains"
                            % (off_axis_source_vis, idx + 1)
                        ):
                            print(
                                "\nSELFCAL No. %s begins: %s min(s) over %s k-lambda ..."
                                % (idx + 1, val, uvrange[len(uvrange) - 1])
                            )
                            if idx == 1:
                                sky_model = os.path.join(
                                    off_axis_source_vis,
                                    "0%s_selfcal" % idx,
                                    "0%s" % len(uvrange),
                                    "_imodel",
                                )
                                pix_val = max(
                                    numpy.genfromtxt(
                                        "%s/0%s_selfcal/0%s/_mf.ilog"
                                        % (
                                            off_axis_source_vis,
                                            idx,
                                            len(uvrange),
                                        ),
                                        invalid_raise=False,
                                        usecols=3,
                                    )
                                )
                            else:
                                sky_model = os.path.join(
                                    off_axis_source_vis,
                                    "0%s_selfcal" % idx,
                                    "_imodel",
                                )
                                pix_val = max(
                                    numpy.genfromtxt(
                                        "%s/0%s_selfcal/_mf.ilog"
                                        % (off_axis_source_vis, idx),
                                        invalid_raise=False,
                                        usecols=3,
                                    )
                                )
                            if pix_val < 0:
                                pix_val = 0
                            if refant is None:
                                if ampcal and idx == len(solint) - 1:
                                    subprocess.run(
                                        [
                                            "selfcal",
                                            "vis=%s" % off_axis_source_vis,
                                            "model=%s" % sky_model,
                                            "clip=%s" % pix_val,
                                            "interval=%s"
                                            % (solint[len(solint) - 1]),
                                            "nfbin=%s" % bins,
                                            "options=mfs,amplitude",
                                        ],
                                        check=True,
                                    )
                                else:
                                    subprocess.run(
                                        [
                                            "selfcal",
                                            "vis=%s" % off_axis_source_vis,
                                            "model=%s" % sky_model,
                                            "clip=%s" % pix_val,
                                            "interval=%s" % val,
                                            "nfbin=%s" % bins,
                                            "options=mfs,phase",
                                        ],
                                        check=True,
                                    )
                            else:
                                if ampcal and idx == len(solint) - 1:
                                    subprocess.run(
                                        [
                                            "selfcal",
                                            "vis=%s" % off_axis_source_vis,
                                            "model=%s" % sky_model,
                                            "clip=%s" % pix_val,
                                            "interval=%s"
                                            % (solint[len(solint) - 1]),
                                            "refant=%s" % refant,
                                            "nfbin=%s" % bins,
                                            "options=mfs,amplitude",
                                        ],
                                        check=True,
                                    )
                                else:
                                    subprocess.run(
                                        [
                                            "selfcal",
                                            "vis=%s" % off_axis_source_vis,
                                            "model=%s" % sky_model,
                                            "clip=%s" % pix_val,
                                            "interval=%s" % val,
                                            "refant=%s" % refant,
                                            "nfbin=%s" % bins,
                                            "options=mfs,phase",
                                        ],
                                        check=True,
                                    )
                            subprocess.run(
                                [
                                    "prthis",
                                    "in=%s" % off_axis_source_vis,
                                    "log=%s/0%s_selfcal/_prthis.log"
                                    % (off_axis_source_vis, idx + 1),
                                ],
                                check=True,
                            )
                            subprocess.run(
                                [
                                    "gpcopy",
                                    "vis=%s" % off_axis_source_vis,
                                    "out=%s/0%s_selfcal"
                                    % (off_axis_source_vis, idx + 1),
                                ],
                                check=True,
                            )
                            if bins == 1:
                                subprocess.run(
                                    ["rm", "%s/gains" % off_axis_source_vis],
                                    check=True,
                                )
                            else:
                                subprocess.run(
                                    [
                                        "rm",
                                        "%s/gains" % off_axis_source_vis,
                                        "%s/gainsf" % off_axis_source_vis,
                                    ],
                                    check=True,
                                )
                    if check_phase:
                        # Assert acceptable RMS before re-imaging
                        rms_phase = _check_phase(
                            "%s/0%s_selfcal/_prthis.log"
                            % (off_axis_source_vis, idx + 1),
                            bins,
                        )
                        if rms_phase > accept_phase:
                            _write_tofile(
                                "SELFCAL No. %s on %s failed!" % (idx + 1, pnt)
                            )
                            sys.exit(
                                "\nRMS of phases exceeded acceptable value!"
                            )
                    # Re-image self-calibrated data
                    if idx == 1:
                        prev_model = os.path.join(
                            off_axis_source_vis,
                            "0%s_selfcal" % idx,
                            "0%s" % len(uvrange),
                            "_%smodel" % stokes,
                        )
                    else:
                        prev_model = os.path.join(
                            off_axis_source_vis,
                            "0%s_selfcal" % idx,
                            "_%smodel" % stokes,
                        )
                    off_axis_source_vis = os.path.join(
                        off_axis_source_vis, "0%s_selfcal" % (idx + 1)
                    )
                    generate_map.imaging(
                        input_dir,
                        model_dir,
                        off_axis_source_vis,
                        stokes,
                        imsize,
                        cell,
                        robust,
                        isigma[1],
                        pol_sigma[1],
                        niters,
                        prev_model,
                        beam,
                    )
                    if check_map_rms:
                        # Check if the selfcal improved the map
                        if idx == 1:
                            initial_file = os.path.join(
                                os.path.split(off_axis_source_vis)[0],
                                "0%s_selfcal/0%s/_%srms.log"
                                % (idx, j + 1, stokes),
                            )
                        else:
                            initial_file = os.path.join(
                                os.path.split(off_axis_source_vis)[0],
                                "0%s_selfcal/_%srms.log" % (idx, stokes),
                            )
                        recent_file = os.path.join(
                            off_axis_source_vis, "_%srms.log" % stokes
                        )
                        initial_map_rms, recent_map_rms = _check_map_rms(
                            initial_file, recent_file
                        )
                        if recent_map_rms >= initial_map_rms:
                            _write_tofile(
                                "SELFCAL No. %s over %s k-lambda on %s failed to improve Stokes %s map!"
                                % (
                                    idx + 1,
                                    uvrange[len(uvrange) - 1],
                                    pnt,
                                    stokes.upper(),
                                )
                            )
                            sys.exit(
                                "\nSELFCAL failed to improve Stokes %s map!"
                                % stokes.upper()
                            )
                    # Pass the directory containing the initial maps for the selfcal to work
                    off_axis_source_vis = os.path.split(off_axis_source_vis)[0]
