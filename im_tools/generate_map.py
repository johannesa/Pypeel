""" 
By default, generates Stokes I image only of the strong off-axis source.
Optionally, the full Stokes images can be generated and also exclude the
Stokes V images.
"""

import os
import subprocess

import numpy


def imaging(
    pre_dir: str,
    model_dir: str,
    off_axis_source_vis: str,
    stokes: str,
    imsize: int,
    cell: float,
    robust: float,
    isigma: tuple[float, float],
    pol_sigma: tuple[float, float],
    niters: int,
    prev_model: str,
    beam: tuple[float, float, float],
):
    """
    Generates models of the strong off-axis source.

    :param pre_dir: Directory containing maps from which the off-axis source
        would be removed.
    :param model_dir: Directory containing model map(s) to use.
    :param off_axis_source_vis: The visibilities containing only the strong
        off-axis source.
    :param stokes: Stokes image of interest.
    :param imsize: Image size in arcsec.
    :param cell: Cell size in arcsec.
    :param robust: Briggs' robust weighting.
    :param isigma: Sigma level threshold for deconvolving Stokes I maps.
    :param pol_sigma: Sigma level threshold for deconvolving polarised maps.
    :param niters: Maximum number of minor iterations for deconvolution.
    :param prev_model: An initial MFCLEAN model for deconvolution.
    :param beam: Restoring beam size (bmaj(arcsec) bmin(arcsec) pa(degrees)).

    :return: None
    """
    psf = os.path.join(off_axis_source_vis, "_%sbeam" % stokes)
    dirty_map = os.path.join(off_axis_source_vis, "_%smap" % stokes)
    model_map = os.path.join(off_axis_source_vis, "_%smodel" % stokes)
    clean_map = os.path.join(off_axis_source_vis, "_%srestor" % stokes)
    if stokes == "i":
        sigma = isigma
    else:
        sigma = pol_sigma

    # We want to create the off-axis region on the dirty map once
    if model_dir is None:
        region_dir = pre_dir
    else:
        region_dir = model_dir
    if os.path.isfile("%s/gains" % region_dir):
        region = os.path.join(pre_dir, "03_uvedit", "cgcurs.region")
    else:
        region = os.path.join(pre_dir, "02_uvedit", "cgcurs.region")

    # Perform inversion
    if not os.path.isdir(dirty_map):
        subprocess.run(
            [
                "invert",
                "vis=%s" % off_axis_source_vis,
                "stokes=%s" % stokes,
                "map=%s" % dirty_map,
                "beam=%s" % psf,
                "imsize=%s" % imsize,
                "cell=%s" % cell,
                "robust=%s" % robust,
                "options=mfs,sdb,double",
            ],
            check=True,
        )

    if not os.path.isfile(region):
        # Create a region around the off-axis source
        subprocess.run(
            ["cgcurs", "in=%s" % dirty_map, "device=/xs", "options=region"],
            check=True,
        )
        subprocess.run(
            ["mv", "cgcurs.region", "%s/cgcurs.region" % off_axis_source_vis],
            check=True,
        )
        # 2nd round of creating a region around the off-axis source
        subprocess.run(
            [
                "cgcurs",
                "in=%s" % dirty_map,
                "device=/xs",
                "options=region",
                "region=@%s/cgcurs.region" % off_axis_source_vis,
            ],
            check=True,
        )
        subprocess.run(
            ["rm", "%s/cgcurs.region" % off_axis_source_vis], check=True
        )
        subprocess.run(
            ["mv", "cgcurs.region", "%s/cgcurs.region" % off_axis_source_vis],
            check=True,
        )
        # 3rd round of creating a region around the off-axis source
        subprocess.run(
            [
                "cgcurs",
                "in=%s" % dirty_map,
                "device=/xs",
                "options=region",
                "region=@%s/cgcurs.region" % off_axis_source_vis,
            ],
            check=True,
        )
        subprocess.run(
            ["rm", "%s/cgcurs.region" % off_axis_source_vis], check=True
        )
        subprocess.run(
            ["mv", "cgcurs.region", "%s/cgcurs.region" % off_axis_source_vis],
            check=True,
        )

    # Perform deconvolution
    if not os.path.isdir(model_map):
        if prev_model is None:
            # Use RMS in dirty map to set CLEAN threshold
            bash_cmd = "sigest in=%s | tee %s.log" % (dirty_map, dirty_map)
            subprocess.run(bash_cmd, shell=True, check=True)
            snr = sigma * float(
                open("%s.log" % dirty_map).readlines()[4].split()[-1]
            )
            print("\nMFCLEAN cut-off set to %.5f Jy ..." % snr)
            subprocess.run(
                [
                    "mfclean",
                    "map=%s" % dirty_map,
                    "beam=%s" % psf,
                    "out=%s" % model_map,
                    "region='@%s'" % region,
                    "cutoff=%s" % snr,
                    "niters=%s" % niters,
                    "log=%s/_mf.%slog" % (off_axis_source_vis, stokes),
                ],
                check=True,
            )
        else:
            # Use RMS in previous CLEAN map to set CLEAN threshold
            snr = sigma * float(
                open(
                    os.path.join(
                        prev_model.split("_%smodel" % stokes)[0],
                        "_%srms.log" % stokes,
                    )
                )
                .readlines()[4]
                .split()[-1]
            )
            print("\nMFCLEAN cut-off set to %.5f Jy ..." % snr)
            subprocess.run(
                [
                    "mfclean",
                    "map=%s" % dirty_map,
                    "beam=%s" % psf,
                    "model=%s" % prev_model,
                    "out=%s" % model_map,
                    "region='@%s'" % region,
                    "cutoff=%s" % snr,
                    "niters=%s" % niters,
                    "log=%s/_mf.%slog" % (off_axis_source_vis, stokes),
                ],
                check=True,
            )
        try:
            flux = numpy.genfromtxt(
                "%s/_mf.%slog" % (off_axis_source_vis, stokes),
                usecols=3,
                invalid_raise=False,
            )
            print("Total CLEANed flux: %.4f Jy" % numpy.sum(flux))
        except ValueError:
            pass

    # Perform restoration
    if not os.path.isdir(clean_map):
        if beam is None:
            subprocess.run(
                [
                    "restor",
                    "model=%s" % model_map,
                    "map=%s" % dirty_map,
                    "beam=%s" % psf,
                    "out=%s" % clean_map,
                ],
                check=True,
            )
        else:
            subprocess.run(
                [
                    "restor",
                    "model=%s" % model_map,
                    "map=%s" % dirty_map,
                    "beam=%s" % psf,
                    "out=%s" % clean_map,
                    "fwhm=%s,%s" % (beam[0], beam[1]),
                    "pa=%s" % beam[2],
                ],
                check=True,
            )

        subprocess.run(
            [
                "cgdisp",
                "in=%s" % clean_map,
                "device=/xs",
                "region='@%s'" % region,
                "options=wedge",
                "range=0,0,lin,8",
            ],
            check=True,
        )

        # Determine the RMS in the CLEAN map
        bash_cmd = "sigest in=%s | tee %s/_%srms.log" % (
            clean_map,
            off_axis_source_vis,
            stokes,
        )
        subprocess.run(bash_cmd, shell=True, check=True)
