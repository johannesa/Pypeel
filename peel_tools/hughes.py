""" Runs Hughes et al. (2007) peeling algorithm """

import os
import subprocess

from utils.check_data import get_off_axis_source_directory


def run(
    input_dir: str,
    off_axis_source_vis: str,
    solint: tuple[float, ...],
    uvrange: tuple,
    all_stokes: bool = False,
    nov: bool = False,
):
    """
    Removes off-axis source from radio maps using the Hughes et al. (2007),
    MNRAS, 382, 543-552

    :param input_dir: Directory containing maps from which the off-axis source
        would be removed
    :param off_axis_source_vis: The visibilities containing only the off-axis
        source
    :param solint: Solution interval (in mins) for self-calibration
    :param uvrange: Sets of UVrange for self-calibration
    :param all_stokes: Perform peeling on all Stokes (I, Q, U, V) maps
    :param nov: Exclude peeling on Stokes V map

    :return: Visibilities with the bright off-axis source removed
    """
    off_axis_dir = get_off_axis_source_directory(
        off_axis_source_vis, uvrange, solint
    )
    if os.path.exists(os.path.join(input_dir, "gains")):
        if not os.path.exists(os.path.join(input_dir, "04_gpcopy")):
            print(
                "\nCopying off-axis source gain solutions to the "
                + "original UV data ..."
            )
            subprocess.run(["mkdir", os.path.join(input_dir, "04_gpcopy")], check=True)
            subprocess.run(
                [
                    "cp",
                    os.path.join(input_dir, "01_uvaver/visdata"),
                    os.path.join(input_dir, "01_uvaver/vartable"),
                    os.path.join(input_dir, "01_uvaver/history"),
                    os.path.join(input_dir, "01_uvaver/header"),
                    os.path.join(input_dir, "01_uvaver/flags"),
                    os.path.join(input_dir, "04_gpcopy"),
                ],
                check=True,
            )
            subprocess.run(
                [
                    "gpcopy",
                    f"vis={off_axis_dir}",
                    f"out={os.path.join(input_dir, '04_gpcopy')}",
                ],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "05_uvaver")):
            print(
                "\nApplying gain solutions for off-axis source "
                + "to the original UV data ..."
            )
            subprocess.run(
                [
                    "uvaver",
                    f"vis={os.path.join(input_dir, '04_gpcopy')}",
                    "stokes=i,q,u,v",
                    f"out={os.path.join(input_dir, '05_uvaver')}",
                ],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "06_uvmodel")):
            print(
                "\nSubtracting off-axis source (Stokes I) from the "
                + "original UV data ..."
            )
            subprocess.run(
                [
                    "uvmodel",
                    f"vis={os.path.join(input_dir, '05_uvaver')}",
                    f"model={os.path.join(off_axis_dir, '_imodel')}",
                    f"out={os.path.join(input_dir, '06_uvmodel_I')}",
                    "options=mfs,subtract",
                ],
                check=True,
            )
            if all_stokes:
                print(
                    "\nSubtracting off-axis source (Stokes Q) from "
                    + "the Stokes-I subtracted data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '06_uvmodel_I')}",
                        f"model={os.path.join(off_axis_dir, '_qmodel')}",
                        f"out={os.path.join(input_dir, '06_uvmodel_Q')}",
                        "options=mfs,subtract",
                    ],
                    check=True,
                )
                print(
                    "\nSubtracting off-axis source (Stokes U) from "
                    + "the Stokes-Q subtracted data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '06_uvmodel_Q')}",
                        f"model={os.path.join(off_axis_dir, '_umodel')}",
                        f"out={os.path.join(input_dir, '06_uvmodel_U')}",
                        "options=mfs,subtract",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "rm",
                        "-r",
                        os.path.join(input_dir, "06_uvmodel_I"),
                        os.path.join(input_dir, "06_uvmodel_Q"),
                    ],
                    check=True,
                )
                if nov:
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "06_uvmodel_U"),
                            os.path.join(input_dir, "06_uvmodel"),
                        ],
                        check=True,
                    )
                else:
                    print(
                        "\nSubtracting off-axis source (Stokes V) "
                        + "from the Stokes-U subtracted data ..."
                    )
                    subprocess.run(
                        [
                            "uvmodel",
                            f"vis={os.path.join(input_dir, '06_uvmodel_U')}",
                            f"model={os.path.join(off_axis_dir, '_vmodel')}",
                            f"out={os.path.join(input_dir, '06_uvmodel_V')}",
                            "options=mfs,subtract",
                        ],
                        check=True,
                    )
                    subprocess.run(
                        ["rm", "-r", os.path.join(input_dir, "06_uvmodel_U")], check=True
                    )
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "06_uvmodel_V"),
                            os.path.join(input_dir, "06_uvmodel"),
                        ],
                        check=True,
                    )
            else:
                # Rename the UV Stokes-I subtracted data
                subprocess.run(
                    [
                        "mv",
                        os.path.join(input_dir, "06_uvmodel_I"),
                        os.path.join(input_dir, "06_uvmodel"),
                    ],
                    check=True,
                )

        if not os.path.exists(os.path.join(input_dir, "06_uvmodel/gains")):
            print(
                "\nCopying off-axis source gain solutions to the off-"
                + "axis-free UV data ..."
            )
            subprocess.run(
                [
                    "gpcopy",
                    f"vis={off_axis_dir}",
                    f"out={os.path.join(input_dir, '06_uvmodel')}",
                ],
                check=True,
            )

            print("\nInverting the gain solutions from off-axis source ...")
            subprocess.run(
                ["gpedit", f"vis={os.path.join(input_dir, '06_uvmodel')}", "options=invert"],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "07_uvaver")):
            print(
                "\nUndoing off-axis source calibrations from the off"
                + "-axis-free UV data ..."
            )
            subprocess.run(
                [
                    "uvaver",
                    f"vis={os.path.join(input_dir, '06_uvmodel')}",
                    "stokes=i,q,u,v",
                    f"out={os.path.join(input_dir, '07_uvaver')}",
                ],
                check=True,
            )
        peeled_vis = os.path.join(input_dir, "07_uvaver")

    else:
        if not os.path.exists(os.path.join(input_dir, "03_gpcopy")):
            print(
                "\nCopying off-axis source gain solutions to the original UV data ..."
            )
            subprocess.run(["mkdir", os.path.join(input_dir, "03_gpcopy")], check=True)
            subprocess.run(
                [
                    "cp",
                    os.path.join(input_dir, "visdata"),
                    os.path.join(input_dir, "vartable"),
                    os.path.join(input_dir, "history"),
                    os.path.join(input_dir, "header"),
                    os.path.join(input_dir, "flags"),
                    os.path.join(input_dir, "03_gpcopy"),
                ],
                check=True,
            )
            subprocess.run(
                [
                    "gpcopy",
                    f"vis={off_axis_dir}",
                    f"out={os.path.join(input_dir, '03_gpcopy')}",
                ],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "04_uvaver")):
            print(
                "\nApplying gain solutions for off-axis source to the original UV data ..."
            )
            subprocess.run(
                [
                    "uvaver",
                    f"vis={os.path.join(input_dir, '03_gpcopy')}",
                    "stokes=i,q,u,v",
                    f"out={os.path.join(input_dir, '04_uvaver')}",
                ],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "05_uvmodel")):
            print(
                "\nSubtracting off-axis source (Stokes I) from the "
                + "original UV data ..."
            )
            subprocess.run(
                [
                    "uvmodel",
                    f"vis={os.path.join(input_dir, '04_uvaver')}",
                    f"model={os.path.join(off_axis_dir, '_imodel')}",
                    f"out={os.path.join(input_dir, '05_uvmodel_I')}",
                    "options=mfs,subtract",
                ],
                check=True,
            )
            if all_stokes:
                print(
                    "\nSubtracting off-axis source (Stokes Q) from "
                    + "the Stokes-I subtracted data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '05_uvmodel_I')}",
                        f"model={os.path.join(off_axis_dir, '_qmodel')}",
                        f"out={os.path.join(input_dir, '05_uvmodel_Q')}",
                        "options=mfs,subtract",
                    ],
                    check=True,
                )
                print(
                    "\nSubtracting off-axis source (Stokes U) from "
                    + "the Stokes-Q subtracted data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '05_uvmodel_Q')}",
                        f"model={os.path.join(off_axis_dir, '_umodel')}",
                        f"out={os.path.join(input_dir, '05_uvmodel_U')}",
                        "options=mfs,subtract",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "rm",
                        "-r",
                        os.path.join(input_dir, "05_uvmodel_I"),
                        os.path.join(input_dir, "05_uvmodel_Q"),
                    ],
                    check=True,
                )
                if nov:
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "05_uvmodel_U"),
                            os.path.join(input_dir, "05_uvmodel"),
                        ],
                        check=True,
                    )
                else:
                    print(
                        "\nSubtracting off-axis source (Stokes V) "
                        + "from the Stokes-U subtracted data ..."
                    )
                    subprocess.run(
                        [
                            "uvmodel",
                            f"vis={os.path.join(input_dir, '05_uvmodel_U')}",
                            f"model={os.path.join(off_axis_dir, '_vmodel')}",
                            f"out={os.path.join(input_dir, '05_uvmodel_V')}",
                            "options=mfs,subtract",
                        ],
                        check=True,
                    )
                    subprocess.run(
                        ["rm", "-r", os.path.join(input_dir, "05_uvmodel_U")], check=True
                    )
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "05_uvmodel_V"),
                            os.path.join(input_dir, "05_uvmodel"),
                        ],
                        check=True,
                    )
            else:
                # Rename the UV Stokes-I subtracted data
                subprocess.run(
                    [
                        "mv",
                        os.path.join(input_dir, "05_uvmodel_I"),
                        os.path.join(input_dir, "05_uvmodel"),
                    ],
                    check=True,
                )

        if not os.path.exists(os.path.join(input_dir, "05_uvmodel/gains")):
            print(
                "\nCopying off-axis source gain solutions to the "
                + "off-axis-free UV data ..."
            )
            subprocess.run(
                [
                    "gpcopy",
                    f"vis={off_axis_dir}",
                    f"out={os.path.join(input_dir, '05_uvmodel')}",
                ],
                check=True,
            )

            print("\nInverting the gain solutions from off-axis source ...")
            subprocess.run(
                ["gpedit", f"vis={os.path.join(input_dir, '05_uvmodel')}", "options=invert"],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "06_uvaver")):
            print(
                "\nUndoing off-axis source calibrations from the off-"
                + "axis-free UV data ..."
            )
            subprocess.run(
                [
                    "uvaver",
                    f"vis={os.path.join(input_dir, '05_uvmodel')}",
                    "stokes=i,q,u,v",
                    f"out={os.path.join(input_dir, '06_uvaver')}",
                ],
                check=True,
            )
        peeled_vis = os.path.join(input_dir, "06_uvaver")

    print("\nPeeling of strong/off-axis source complete!")

    return peeled_vis
