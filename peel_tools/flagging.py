"""
For flagging visibilities after peeling of bright off-axis source(s)
"""

import subprocess


class FlagVis:
    """
    Class for automated flagging with PGFLAG or manual flagging with BLFLAG
    """

    def __init__(self, vis: str, all_stokes=False, nov=False) -> None:
        """
        Flags visibilities manually or in an automatic fashion

        :param vis: Path to MIRIAD visibilities. These visibilities have
            the bright off-axis source removed
        :param all_stokes: If True, all stokes maps (I, Q, U, and optionally V)
            are flagged
        :param nov: If True, Stokes V maps are not flagged. In this case,
            Stokes V maps are not peeled at all

        :return: None
        """
        self.vis = vis
        self.all_stokes = all_stokes
        self.nov = nov

    def auto(self, iflagpar: tuple[int], polflagpar: tuple[int]) -> None:
        """
        Automatic flags visibilities with PGFLAG

        :param iflagpar: SumThreshold flagging parameters for flagging
           the Stokes I component of the visibilities
        :param polflagpar: SumThreshold flagging parameters for flagging
           polarised components of the visibilities

        :return: None
        """
        print("\nAutomatic flagging with PGFLAG...")
        try:
            print("Flagging in Stokes I...")
            subprocess.run(
                [
                    "pgflag",
                    f"vis={self.vis}",
                    f"flagpar={iflagpar}",
                    "device=/xs",
                    "stokes=v,q,u,i",
                    "command='<b'",
                    "mode=amplitude",
                ],
                check=True,
            )
            if self.all_stokes:
                print("Flagging in Stokes Q...")
                subprocess.run(
                    [
                        "pgflag",
                        f"vis={self.vis}",
                        f"flagpar={polflagpar}",
                        "device=/xs",
                        "stokes=u,v,q",
                        "command='<b'",
                        "mode=amplitude",
                    ],
                    check=True,
                )

                print("Flagging in Stokes U...")
                subprocess.run(
                    [
                        "pgflag",
                        f"vis={self.vis}",
                        f"flagpar={polflagpar}",
                        "device=/xs",
                        "stokes=v,q,u",
                        "command='<b'",
                        "mode=amplitude",
                    ],
                    check=True,
                )
                if not self.nov:
                    print("Flagging in Stokes V...")
                    subprocess.run(
                        [
                            "pgflag",
                            f"vis={self.vis}",
                            f"flagpar={polflagpar}",
                            "device=/xs",
                            "stokes=i,q,u,v",
                            "command='<b'",
                            "mode=amplitude",
                        ],
                        check=True,
                    )
        except KeyboardInterrupt:
            print("KeyboardInterrupt except is caught!")

    def manual(self) -> None:
        """
        Manual flagging of visibilities with BLFLAG

        :return: None
        """
        print("Manual flagging with BLFLAG...")
        try:
            print("Flagging in Stokes I...")
            subprocess.run(
                [
                    "blflag",
                    f"vis={self.vis}",
                    "device=/xs",
                    "stokes=i",
                    "axis=time,amp",
                    "options=nobase",
                ],
                check=True,
            )
            subprocess.run(
                [
                    "blflag",
                    f"vis={self.vis}",
                    "device=/xs",
                    "stokes=i",
                    "axis=channel,amp",
                    "options=nobase",
                ],
                check=True,
            )
            subprocess.run(
                [
                    "blflag",
                    f"vis={self.vis}",
                    "device=/xs",
                    "stokes=i",
                    "axis=real,imag",
                    "options=nobase",
                ],
                check=True,
            )
            subprocess.run(
                [
                    "blflag",
                    f"vis={self.vis}",
                    "device=/xs",
                    "stokes=i",
                    "axis=uvdist,amp",
                    "options=nobase",
                ],
                check=True,
            )

            if self.all_stokes:
                print("Flagging in Stokes Q...")
                subprocess.run(
                    [
                        "blflag",
                        f"vis={self.vis}",
                        "device=/xs",
                        "stokes=q",
                        "axis=time,amp",
                        "options=nobase",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "blflag",
                        f"vis={self.vis}",
                        "device=/xs",
                        "stokes=q",
                        "axis=channel,amp",
                        "options=nobase",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "blflag",
                        f"vis={self.vis}",
                        "device=/xs",
                        "stokes=q",
                        "axis=real,imag",
                        "options=nobase",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "blflag",
                        f"vis={self.vis}",
                        "device=/xs",
                        "stokes=q",
                        "axis=uvdist,amp",
                        "options=nobase",
                    ],
                    check=True,
                )

                print("Flagging in Stokes U...")
                subprocess.run(
                    [
                        "blflag",
                        f"vis={self.vis}",
                        "device=/xs",
                        "stokes=u",
                        "axis=time,amp",
                        "options=nobase",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "blflag",
                        f"vis={self.vis}",
                        "device=/xs",
                        "stokes=u",
                        "axis=channel,amp",
                        "options=nobase",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "blflag",
                        f"vis={self.vis}",
                        "device=/xs",
                        "stokes=u",
                        "axis=real,imag",
                        "options=nobase",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "blflag",
                        f"vis={self.vis}",
                        "device=/xs",
                        "stokes=u",
                        "axis=uvdist,amp",
                        "options=nobase",
                    ],
                    check=True,
                )
                if not self.nov:
                    print("Flagging in Stokes V ...")
                    subprocess.run(
                        [
                            "blflag",
                            f"vis={self.vis}",
                            "device=/xs",
                            "stokes=v",
                            "axis=time,amp",
                            "options=nobase",
                        ],
                        check=True,
                    )
                    subprocess.run(
                        [
                            "blflag",
                            f"vis={self.vis}",
                            "device=/xs",
                            "stokes=v",
                            "axis=channel,amp",
                            "options=nobase",
                        ],
                        check=True,
                    )
                    subprocess.run(
                        [
                            "blflag",
                            f"vis={self.vis}",
                            "device=/xs",
                            "stokes=v",
                            "axis=real,imag",
                            "options=nobase",
                        ],
                        check=True,
                    )
                    subprocess.run(
                        [
                            "blflag",
                            f"vis={self.vis}",
                            "device=/xs",
                            "stokes=v",
                            "axis=uvdist,amp",
                            "options=nobase",
                        ],
                        check=True,
                    )
        except KeyboardInterrupt:
            print("KeyboardInterrupt except is caught!")
