""" Runs Butler et al. (2018) peeling algorithm """

import os
import subprocess

from utils.check_data import get_off_axis_source_directory


def run(
    input_dir: str,
    off_axis_source_vis: str,
    masked_model: list,
    solint: tuple[float, ...],
    uvrange: tuple,
    all_stokes: bool,
    nov: bool,
):
    """
    Removes off-axis source from radio maps using the Butler et al. (2018),
    A&A, 620, A3.

    :param input_dir: Directory containing maps from which the off-axis source
        would be removed.
    :param off_axis_source_vis: The visibilities containing only the off-axis
        source.
    :param masked_model: The model maps with the off-axis source masked. Hence,
        contains only the science targets.
    :param solint: Solution interval (in mins) for self-calibration.
    :param uvrange: Sets of UVrange for self-calibration.
    :param all_stokes: Perform peeling on all Stokes (I, Q, U, V) maps.
    :param nov: Exclude peeling on Stokes V map.

    :return: Visibilities with the bright off-axis source removed.
    """
    off_axis_dir = get_off_axis_source_directory(
        off_axis_source_vis, uvrange, solint
    )
    if os.path.exists(os.path.join(input_dir, "gains")):
        if not os.path.exists(os.path.join(input_dir, "04_uvmodel")):
            print(
                "\nSubtracting off-axis source (Stokes I) from the "
                + "original UV data ..."
            )
            subprocess.run(
                [
                    "uvmodel",
                    f"vis={os.path.join(input_dir, '01_uvaver')}",
                    f"model={os.path.join(off_axis_dir, '_imodel')}",
                    f"out={os.path.join(input_dir, '04_uvmodel_I')}",
                    "options=mfs,subtract",
                ],
                check=True,
            )
            if all_stokes:
                print(
                    "\nSubtracting off-axis source (Stokes Q) from the "
                    + "Stokes-I subtracted data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '04_uvmodel_I')}",
                        f"model={os.path.join(off_axis_dir, '_qmodel')}",
                        f"out={os.path.join(input_dir, '04_uvmodel_Q')}",
                        "options=mfs,subtract",
                    ],
                    check=True,
                )
                print(
                    "\nSubtracting off-axis source (Stokes U) from the "
                    + "Stokes-Q subtracted data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '04_uvmodel_Q')}",
                        f"model={os.path.join(off_axis_dir, '_umodel')}",
                        f"out={os.path.join(input_dir, '04_uvmodel_U')}",
                        "options=mfs,subtract",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "rm",
                        "-r",
                        os.path.join(input_dir, "04_uvmodel_I"),
                        os.path.join(input_dir, "04_uvmodel_Q"),
                    ],
                    check=True,
                )
                if nov:
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "04_uvmodel_U"),
                            os.path.join(input_dir, "04_uvmodel"),
                        ],
                        check=True,
                    )
                else:
                    print(
                        "\nSubtracting off-axis source (Stokes V) from the "
                        + "Stokes-U subtracted data ..."
                    )
                    subprocess.run(
                        [
                            "uvmodel",
                            f"vis={os.path.join(input_dir, '04_uvmodel_U')}",
                            f"model={os.path.join(off_axis_dir, '_vmodel')}",
                            f"out={os.path.join(input_dir, '04_uvmodel_V')}",
                            "options=mfs,subtract",
                        ],
                        check=True,
                    )
                    subprocess.run(
                        ["rm", "-r", os.path.join(input_dir, "04_uvmodel_U")], check=True
                    )
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "04_uvmodel_V"),
                            os.path.join(input_dir, "04_uvmodel"),
                        ],
                        check=True,
                    )
            else:
                subprocess.run(
                    [
                        "mv",
                        os.path.join(input_dir, "04_uvmodel_I"),
                        os.path.join(input_dir, "04_uvmodel"),
                    ],
                    check=True,
                )

        if not os.path.exists(os.path.join(input_dir, "04_uvmodel/gains")):
            print(
                "\nCopying off-axis source gain solutions to the off-axis free UV data ..."
            )
            subprocess.run(
                [
                    "gpcopy",
                    f"vis={off_axis_dir}",
                    f"out={os.path.join(input_dir, '04_uvmodel')}",
                ],
                check=True,
            )

            print(
                "\nInverting the gain solutions from the off-axis source ..."
            )
            subprocess.run(
                ["gpedit", f"vis={os.path.join(input_dir, '04_uvmodel')}", "options=invert"],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "05_uvaver")):
            print(
                "\nUndoing the off-axis source calibrations from the off-axis free UV data ..."
            )
            subprocess.run(
                [
                    "uvaver",
                    f"vis={os.path.join(input_dir, '04_uvmodel')}",
                    "stokes=i,q,u,v",
                    f"out={os.path.join(input_dir, '05_uvaver')}",
                ],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "06_uvmodel")):
            if all_stokes:
                for idx, im in enumerate(zip(masked_model)):
                    if idx == 0:
                        print(
                            "\nAdding back Stokes I science targets to "
                            + "the off-axis free data ..."
                        )
                        subprocess.run(
                            [
                                "uvmodel",
                                f"vis={os.path.join(input_dir, '05_uvaver')}",
                                f"model={im}",
                                f"out={os.path.join(input_dir, '06_uvmodel_%s' % idx)}",
                                "options=mfs,add",
                            ],
                            check=True,
                        )
                    else:
                        if idx == 1:
                            print(
                                "\nAdding back Stokes Q science targets "
                                + "to the off-axis free data ..."
                            )
                        elif idx == 2:
                            print(
                                "\nAdding back Stokes U science targets "
                                + "to the off-axis free data ..."
                            )
                        try:
                            if idx == 3:
                                print(
                                    "\nAdding back Stokes V science targets "
                                    + "to the off-axis free data ..."
                                )
                        except:
                            pass
                        subprocess.run(
                            [
                                "uvmodel",
                                f"vis={os.path.join(input_dir, '06_uvmodel_%s' % (idx - 1))}",
                                f"model={im}",
                                f"out={os.path.join(input_dir, '06_uvmodel_%s' % idx)}",
                                "options=mfs,add",
                            ],
                            check=True,
                        )
                subprocess.run(
                    [
                        "rm",
                        "-r",
                        os.path.join(input_dir, "06_uvmodel_0"),
                        os.path.join(input_dir, "06_uvmodel_1"),
                    ],
                    check=True,
                )
                if nov:
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "06_uvmodel_2"),
                            os.path.join(input_dir, "06_uvmodel"),
                        ],
                        check=True,
                    )
                else:
                    subprocess.run(
                        ["rm", "-r", os.path.join(input_dir, "06_uvmodel_2")], check=True
                    )
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "06_uvmodel_3"),
                            os.path.join(input_dir, "06_uvmodel"),
                        ],
                        check=True,
                    )
            else:
                print(
                    "\nAdding back Stokes I science targets to the off-"
                    + "axis-free data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '05_uvaver')}",
                        f"model={masked_model}",
                        f"out={os.path.join(input_dir, '06_uvmodel')}",
                        "options=mfs,add",
                    ],
                    check=True,
                )
        peeled_vis = os.path.join(input_dir, "06_uvmodel")

    else:
        if not os.path.exists(os.path.join(input_dir, "03_uvmodel")):
            print(
                "\nSubtracting off-axis source (Stokes I) from the "
                + "original UV data ..."
            )
            subprocess.run(
                [
                    "uvmodel",
                    f"vis={input_dir}",
                    f"model={os.path.join(off_axis_dir, '_imodel')}",
                    f"out={os.path.join(input_dir, '03_uvmodel_I')}",
                    "options=mfs,subtract",
                ],
                check=True,
            )
            if all_stokes:
                print(
                    "\nSubtracting off-axis source (Stokes Q) from the "
                    + "Stokes-I subtracted data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '03_uvmodel_I')}",
                        f"model={os.path.join(off_axis_dir, '_qmodel')}",
                        f"out={os.path.join(input_dir, '03_uvmodel_Q')}",
                        "options=mfs,subtract",
                    ],
                    check=True,
                )
                print(
                    "\nSubtracting off-axis source (Stokes U) from the "
                    + "Stokes-Q subtracted data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '03_uvmodel_Q')}",
                        f"model={os.path.join(off_axis_dir, '_umodel')}",
                        f"out={os.path.join(input_dir, '03_uvmodel_U')}",
                        "options=mfs,subtract",
                    ],
                    check=True,
                )
                subprocess.run(
                    [
                        "rm",
                        "-r",
                        os.path.join(input_dir, "03_uvmodel_I"),
                        os.path.join(input_dir, "03_uvmodel_Q"),
                    ],
                    check=True,
                )
                if nov:
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "03_uvmodel_U"),
                            os.path.join(input_dir, "03_uvmodel"),
                        ],
                        check=True,
                    )
                else:
                    print(
                        "\nSubtracting off-axis source (Stokes V) from the "
                        + "Stokes-U subtracted data ..."
                    )
                    subprocess.run(
                        [
                            "uvmodel",
                            f"vis={os.path.join(input_dir, '03_uvmodel_U')}",
                            f"model={os.path.join(off_axis_dir, '_vmodel')}",
                            f"out={os.path.join(input_dir, '03_uvmodel_V')}",
                            "options=mfs,subtract",
                        ],
                        check=True,
                    )
                    subprocess.run(
                        ["rm", "-r", os.path.join(input_dir, "03_uvmodel_U")], check=True
                    )
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "03_uvmodel_V"),
                            os.path.join(input_dir, "03_uvmodel"),
                        ],
                        check=True,
                    )
            else:
                subprocess.run(
                    [
                        "mv",
                        os.path.join(input_dir, "03_uvmodel_I"),
                        os.path.join(input_dir, "03_uvmodel"),
                    ],
                    check=True,
                )

        if not os.path.exists(os.path.join(input_dir, "03_uvmodel/gains")):
            print(
                "\nCopying off-axis source gain solutions to the off-axis free UV data ..."
            )
            subprocess.run(
                [
                    "gpcopy",
                    f"vis={off_axis_dir}",
                    f"out={os.path.join(input_dir, '03_uvmodel')}",
                ],
                check=True,
            )

            print(
                "\nInverting the gain solutions from the off-axis source ..."
            )
            subprocess.run(
                ["gpedit", f"vis={os.path.join(input_dir, '03_uvmodel')}", "options=invert"],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "04_uvaver")):
            print(
                "\nUndoing the off-axis source calibrations from the off-axis free UV data ..."
            )
            subprocess.run(
                [
                    "uvaver",
                    f"vis={os.path.join(input_dir, '03_uvmodel')}",
                    "stokes=i,q,u,v",
                    f"out={os.path.join(input_dir, '04_uvaver')}",
                ],
                check=True,
            )

        if not os.path.exists(os.path.join(input_dir, "05_uvmodel")):
            if all_stokes:
                for idx, im in enumerate(zip(masked_model)):
                    if idx == 0:
                        print(
                            "\nAdding back Stokes I science targets to "
                            + "the off-axis free data ..."
                        )
                        subprocess.run(
                            [
                                "uvmodel",
                                f"vis={os.path.join(input_dir, '04_uvaver')}",
                                f"model={im}",
                                f"out={os.path.join(input_dir, '05_uvmodel_%s' % idx)}",
                                "options=mfs,add",
                            ],
                            check=True,
                        )
                    else:
                        if idx == 1:
                            print(
                                "\nAdding back Stokes Q science targets "
                                + "to the off-axis free data ..."
                            )
                        elif idx == 2:
                            print(
                                "\nAdding back Stokes U science targets "
                                + "to the off-axis free data ..."
                            )
                        try:
                            if idx == 3:
                                print(
                                    "\nAdding back Stokes V science targets "
                                    + "to the off-axis free data ..."
                                )
                        except:
                            pass
                        subprocess.run(
                            [
                                "uvmodel",
                                f"vis={os.path.join(input_dir, '05_uvmodel_%s' % (idx - 1))}",
                                f"model={im}",
                                f"out={os.path.join(input_dir, '05_uvmodel_%s' % idx)}",
                                "options=mfs,add",
                            ],
                            check=True,
                        )
                subprocess.run(
                    [
                        "rm",
                        "-r",
                        os.path.join(input_dir, "05_uvmodel_0"),
                        os.path.join(input_dir, "05_uvmodel_1"),
                    ],
                    check=True,
                )
                if nov:
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "05_uvmodel_2"),
                            os.path.join(input_dir, "05_uvmodel"),
                        ],
                        check=True,
                    )
                else:
                    subprocess.run(
                        ["rm", "-r", os.path.join(input_dir, "05_uvmodel_2")], check=True
                    )
                    subprocess.run(
                        [
                            "mv",
                            os.path.join(input_dir, "05_uvmodel_3"),
                            os.path.join(input_dir, "05_uvmodel"),
                        ],
                        check=True,
                    )
            else:
                print(
                    "\nAdding back Stokes I science targets to the off-"
                    + "axis-free data ..."
                )
                subprocess.run(
                    [
                        "uvmodel",
                        f"vis={os.path.join(input_dir, '04_uvaver')}",
                        f"model={masked_model}",
                        f"out={os.path.join(input_dir, '05_uvmodel')}",
                        "options=mfs,add",
                    ],
                    check=True,
                )

        peeled_vis = os.path.join(input_dir, "05_uvmodel")

    print("\nPeeling of strong/off-axis source complete!")

    return peeled_vis
