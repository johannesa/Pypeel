"""
Image-based implementation of popular simple peeling techniques - to 
remove off-axis sources from radio images.
"""

import argparse
import datetime
import time

from im_tools.selfcal import calibrate_and_image
from peel_tools.flagging import FlagVis
from utils.remove_source import PeelTools
from utils.check_data import check_peeling_data
from utils.get_off_axis_source import src_position
from utils.get_phase_centre import phase_centre


def main():
    """Get command line arguments"""
    parser = argparse.ArgumentParser(
        description="Tool for removing strong off-axis sources from radio maps using MIRIAD."
    )
    # Input directory
    parser.add_argument(
        "--input_dir",
        help="Pre-peeling directory: Input directory containing visibilities, "
        "model map(s) and/or calibration solutions from which the off-axis "
        "source would be removed.",
        type=str,
    )
    parser.add_argument(
        "--model_dir",
        help="Directory containing model(s) to mask off-axis source for peeling.",
        type=str,
    )

    # Peeling parameters
    parser.add_argument(
        "--alg",
        help="Peeling algorithm to use. [default: allotey]",
        type=str,
        choices=("allotey", "butler", "hughes"),
        default="allotey",
    )
    parser.add_argument(
        "--all_stokes",
        action="store_true",
        help="Peel sources in all Stokes i.e I, Q, U and V. "
        + "[default: Stokes I only]",
        default=False,
    )
    parser.add_argument(
        "--nov",
        action="store_true",
        help="Excludes peeling in Stokes V when peeling in all "
        + "Stokes. [default: False]",
        default=False,
    )

    # Parameters for flagging peeled visibilities
    parser.add_argument(
        "--flag",
        action="store_true",
        help="Flag visibilities after removing the off-axis source [default: False]",
        default=False,
    )
    parser.add_argument(
        "--auto",
        action="store_true",
        help="Automatic flagging of peeled visibilities with "
        + "PGFLAG. [default: False for manual flagging with "
        + "manual flagging]",
        default=False,
    )
    parser.add_argument(
        "--iflagpar",
        help="Parameters for SumThreshold flagging, dusting, and extending in "
        + "Stokes I . Refer to the documentation on PGFLAG for description "
        + "of the input parameters. [default:8 5 5 3 6 3]",
        type=str,
        default="8, 5, 5, 3, 6, 3",
    )

    parser.add_argument(
        "--polflagpar",
        help="Parameters for SumThreshold flagging, dusting, and extending in "
        + "polarisation. Refer to the documentation on PGFLAG for description "
        + "of the input parameters. [default:8 5 5 3 6 3]",
        type=str,
        default="8, 2, 2, 3, 6, 3",
    )

    # Self-calibration parameters
    parser.add_argument(
        "--ampcal",
        action="store_true",
        help="Perform amplitude selfcal. [default: False]",
        default=False,
    )
    parser.add_argument(
        "--solint",
        help="Time interval(s) for selfcal (in mins). If "
        + "used with amplitude self-calibration, the last "
        + "value is used for the amplitude self-calibration "
        + "[No default]",
        nargs="+",
    )
    parser.add_argument(
        "--refant",
        help="Reference antenna for selfcal [default: Antenna "
        + "with the greatest weight]",
        type=int,
    )
    parser.add_argument(
        "--bins",
        help="Frequency bins to compute selfcal solutions "
        + "with task SELFCAL [Default: 1]",
        type=int,
        default=1,
    )
    parser.add_argument(
        "--uvrange",
        help="UVrange to use for first self-calibration only. "
        + "Specify from shortest to longest baseline in kilo-lambda. "
        + "Eg. 20,65 10,65 0,65 [Default: Full UVrange]",
        type=str,
        nargs="+",
        default=None,
    )
    parser.add_argument(
        "--accept_phase",
        help="The acceptable RMS of gain phases after selfcal "
        + "above which the solutions are rejected (in degrees). "
        + "[Default: 15.0]",
        type=float,
        default=15.0,
    )

    # Inversion parameters
    parser.add_argument(
        "--imsize",
        help="Output image size of the off-axis source (in arcsec). [default: 512]",
        type=int,
        default=512,
    )
    parser.add_argument(
        "--cell",
        help="Image (of the off-axis source) cell size (in arcsec). [default: 1.0]",
        type=float,
        default=1.0,
    )
    parser.add_argument(
        "--robust",
        help="Briggs' robust weighting parameter. [default: 0.0]",
        type=float,
        default=0.0,
    )

    # Deconvolution parameters
    parser.add_argument(
        "--niters",
        help="Maximum number of minor iterations for deconvolution. [default: 10000]",
        type=int,
        default=10000,
    )
    parser.add_argument(
        "--i_sigma",
        help="Sigma levels for deconvolving initial and self-"
        + "calibrated Stokes I map respectively. [default: 0 0]",
        type=float,
        nargs=2,
        default=(0.0, 0.0),
    )
    parser.add_argument(
        "--pol_sigma",
        help="Sigma levels for deconvolving initial and self-"
        + "calibrated polarised maps respectively. [default: 0 0]",
        type=float,
        nargs=2,
        default=(0.0, 0.0),
    )
    parser.add_argument(
        "--gain",
        help="Minor iteration loop gain for deconvolution. [default: 0.1]",
        type=float,
        default=0.1,
    )

    # Restoration parameters
    parser.add_argument(
        "--beam",
        help="Restoring beam as bmaj(arcsec) bmin(arcsec) bpa(deg)). [default: "
        "Fits a Gaussian to the main lobe of the dirty beam]",
        type=str,
        nargs=3,
    )

    # Quality assessment parameter
    parser.add_argument(
        "--check_map_rms",
        action="store_true",
        help="Check CLEAN map of the off-axis source improves after each "
        "selfcal. [default: False]",
        default=False,
    )
    parser.add_argument(
        "--check_phase",
        action="store_true",
        help="Check RMS of phases after selfcal is better "
        + "than the acceptable RMS value set by the --accept "
        + "argument. [default: False]",
        default=False,
    )
    args = parser.parse_args()

    begin = time.time()
    if args.check_phase:
        print(
            "Requested to check RMS of gain phases self-calibration is better than %s degree(s)..."
            % args.accept_phase
        )
    if args.check_map_rms:
        print(
            "Requested to check maps improve after each round of self-calibration..."
        )
    if args.uvrange:
        print(
            "Self-calibrate over %s set(s) of UVrange ..." % len(args.uvrange)
        )
    if not args.solint:
        raise ValueError("Provide solution intervals using --solint argument!")
    if args.ampcal and len(args.solint) == 1:
        raise ValueError(
            "Performs phase-only first before phase and amplitude so "
            + "increase number of solution intervals!"
        )

    # Ensure the datadir contains all the needed files for peeling
    check_peeling_data(
        args.input_dir, args.model_dir, args.all_stokes, args.nov
    )

    # Get the observation phase centre
    obs_ra, obs_dec = phase_centre(args.input_dir)

    # Get the position of the off-axis source
    off_axis_source_vis, masked_model = src_position(
        args.input_dir, args.model_dir, args.all_stokes, args.nov
    )

    # Generate model(s) of the off-axis source by self-calibration
    # and re-imaging
    calibrate_and_image(
        args.input_dir,
        args.model_dir,
        off_axis_source_vis,
        args.solint,
        args.uvrange,
        args.refant,
        args.bins,
        args.accept_phase,
        args.imsize,
        args.cell,
        args.robust,
        args.i_sigma,
        args.pol_sigma,
        args.niters,
        args.beam,
        args.all_stokes,
        args.nov,
        args.ampcal,
        args.check_map_rms,
        args.check_phase,
    )

    # Choose from one of the available peeling algorithms
    peel_init = PeelTools(
        args.input_dir,
        off_axis_source_vis,
        masked_model,
        args.solint,
        args.uvrange,
        args.all_stokes,
        args.nov,
    )

    if args.alg == "allotey":
        print("\nPeeling algorithm requested: Allotey et al. (in prep.)")
        peeled_vis = peel_init.allotey(obs_ra, obs_dec)
    elif args.alg == "butler":
        print("\nPeeling algorithm requested: Butler et al. (2016)")
        peeled_vis = peel_init.butler()
    elif args.alg == "hughes":
        print("\nPeeling algorithm requested: Hughes et al. (2007)")
        peeled_vis = peel_init.hughes()
    else:
        raise ValueError("Unknown algorithm requested!")

    # Finally, flag the visibilities - the bright off-axis source has
    # been removed
    if args.flag:
        flag_init = FlagVis(peeled_vis, args.all_stokes, args.nov)
        if args.auto:
            flag_init.auto(args.iflagpar, args.polflagpar)
        else:
            flag_init.manual()

    end = time.time()
    print(
        "\nProcess finished in %s"
        % str(datetime.timedelta(seconds=end - begin))
    )


if __name__ == "__main__":
    main()
