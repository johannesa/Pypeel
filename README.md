### Features
- `Pypeel`: Peels off-axis sources from radio maps with [MIRIAD](https://www.atnf.csiro.au/computing/software/miriad/).
- `sfind`: Source finds on the pre and post peeling maps with [PyBDSF](https://github.com/lofar-astron/PyBDSF).
- `compare`: Compares the quality of the peeling algorithm used.


# Pypeel
`Pypeel` is a semi-automated python tool for performing image-based peeling of off-axis
sources from radio maps with [MIRIAD](https://www.atnf.csiro.au/computing/software/miriad/). 
Since sources may be polarised, the `allstokes` argument can be used to peel sources in
all Stokes i.e the sources would be peeled in Stokes I, Q, U, V maps by default, and 
optionally exclude the Stokes V map with the `nov` argument. 

By default, `Pypeel` allows you to create a region around the off-axis source on the CLEAN 
map (contains both the science targets and the off-axis source) and fits for the position 
of the off-axis source with `IMFIT` for masking out this off-axis source in the model map(s).
`Pypeel` offers three rounds to do this in order to create compact regions around the source.
Alternatively, a [MIRIAD](https://www.atnf.csiro.au/computing/software/miriad/) readable
region file could be created separately, labelled as `cgcurs.region` and placed in the 
pre-peeling directory.

After the off-axis source is masked out in the model map(s), `Pypeel` offers three rounds
to create a region around the off-axis source in the dirty map containing only this source.
`SELFCAL` and `MFCLEAN` are used for self-calibration and deconvolution respectively 
to generate model(s) of the off-axis source. 

For any of the algorithms used, the output directories are numbered and the
[MIRIAD](https://www.atnf.csiro.au/computing/software/miriad/) task used is appended to it -
a probably easy way to read the tasks applied. The final output directory contains the off-axis
free visibilities to image for your science analysis. In some cases, a CLEAN map might contain
multiple off-axis sources - the routine is to peel sources in descending order of flux.
There are two approaches to peel multiple sources: 

(a) Peel the strongest source and image the off-axis free visibilities. This would generate 
models and CLEAN maps). Peel the next strongest source and image the new off-axis 
free visibilities and repeat the process until all off-axis sources are removed. Note that, 
masking out of off-axis sources in each process is done on the newly generated model maps 
after imaging. This approach can be time consuming - increases with the number of sources 
to be peeled since new models have to be generated after each round of peeling. 

(b) Peel the strongest source but do not image the off-axis free visibilities. This off-axis 
free visibilities would become the new pre-peeling directory, and the model(s) one started 
with are the same to be used for the next rounds of peeling. Note that a record of each 
round of model masking is kept, so any peeled source won't be added back in the subsequent 
stages of the process. This approach saves time since imaging of off-axis free visibilities
is done once i.e after the final round of peeling. By default, `Pypeel` looks for the model 
and CLEAN map(s) in the pre-peeling directory, so the `model_dir` argument can be used to
redirect `Pypeel` where to look for these map(s) while changing the pre-peeling directory
after every round of peeling.

The implemented algorithms are:
1. Allotey et al. (in prep.)
2. [Butler et al. (2018)](https://www.aanda.org/articles/aa/full_html/2018/12/aa30129-16/aa30129-16.html)
3. [Hughes et al. (2007)](https://academic.oup.com/mnras/article/382/2/543/1027290) 



# sfind
`sfind` runs [PyBDSF](https://www.astron.nl/citt/pybdsf/) on the inner third of
the pre and post peeling maps -  it is accessible from the `utils` directory.
This feature writes source catalogues to allow for comparing the effect of the
peeling algorithm on the science targets. The gaussian, source, and region list are
saved in BBS, CSV, and ds9 formats respectively. Since all of the source finder's
features are not accessible in `sfind`, one can independently source find on the
maps for later comparison. 



# compare
`compare` cross-matches pre and post -peeling source catalogues to within a user
specified separation and compares the flux density of the sources. It is recommended
the source catalogues are generated from the inner third of the maps. The tool is accessible
from the `utils` directory. The catalogues should be in CSV format and the Right Ascension,
Declination, Integrated flux, Error in integrated flux, Peak flux, and Error in peak flux
should be labelled as RA, DEC, Total_flux, E_Total_flux, Peak_flux, E_Peak_flux. This is
a very simple cross-matching tool so the source catalogues should be refined before using
this tool. 



## Installation
For peeling only, install [MIRIAD](https://www.atnf.csiro.au/computing/software/miriad/).
To inspect and compare the quality of the peeling algorithms, install
[PyBDSF](https://www.astron.nl/citt/pybdsf/). Finally, clone this repository and make
use of any feature of interest. 



## Help
If you find a bug and/or you have a need for another technique to be
implemented, please [submit a ticket](https://gitlab.com/johannesa/Pypeel/-/issues)
and they/it will be attended to.
