"""
Source finds with PyBDSF in the inner third of the pre and post -peeling
maps to compare the quality of the peeling algorithm.
"""

import argparse
import datetime
import os
import time

import bdsf
from astropy.io import fits as pyfits


def check_images(
    pre_img: str, post_img: str
) -> tuple[float, float, float, int, int]:
    """
    Checks pre and post -peeling maps have the same restoring beam size.

    :param pre_img: Pre-peeling FITS map.
    :param post_img: Post-peeling FITS map.

    :return: The restoring beam and image sizes.
    """
    # Check if the restoring beams are the same
    pre_hdr = pyfits.getheader(pre_img)
    post_hdr = pyfits.getheader(post_img)

    # Ensure the pre and post peeling maps restoring beam sizes are the same
    assert (
        pre_hdr["BMAJ"] == post_hdr["BMAJ"]
    ), "Pre and post -peeling images must have same beam major axis size!"
    assert (
        pre_hdr["BMIN"] == post_hdr["BMIN"]
    ), "Pre and post -peeling images must have same beam minor axis size!"
    try:
        assert (
            pre_hdr["BPA"] == post_hdr["BPA"]
        ), "Pre and post -peeling images must have the same position angle!"
        pos_angle = pre_hdr["BPA"]
    except KeyError:
        pos_angle = 0.0

    # Ensure the pre and post -peeling images sizes are the same
    assert (
        pre_hdr["NAXIS1"] == post_hdr["NAXIS1"]
    ), "Pre and post peeling images NAXIS1 must be the same!"
    assert (
        pre_hdr["NAXIS2"] == post_hdr["NAXIS2"]
    ), "Pre and post peeling images NAXIS2 must be the same!"

    return (
        pre_hdr["BMAJ"],
        pre_hdr["BMIN"],
        pos_angle,
        pre_hdr["NAXIS1"],
        pre_hdr["NAXIS2"],
    )


class ProcessImage:
    """
    Collates PyBDSF parameters for source finding.

    :param trim_box: Bounding box within source finding should be
        performed.
    :param rms_box: Bounding box for RMS and mean map calculation.
    :param rms_box_bright: Bounding box for RMS and mean map
        calculation near bright sources
    :param threshold: Source detection threshold.
    :param island: Threshold for island boundary above the mean.
    :param group_tol: Tolerance for grouping Gaussians into sources.
    :param atrous_jmax: Maximum allowed wavelength order for wavelet
        decomposition.
    :param shapelet_do: Use the shapelet decomposition module.
    :param atrous_do: Use the wavelet decomposition module.
    :param beam: The restoring beam size.
    """

    def __init__(
        self,
        trim_box: tuple[float, float, float, float],
        rms_box: tuple[float, float],
        rms_box_bright: tuple[float, float],
        threshold: float,
        island: float,
        group_tol: float,
        atrous_jmax: int = 0,
        shapelet_do: bool = False,
        atrous_do: bool = False,
        beam=None,
    ):
        self.trim_box = trim_box
        self.rms_box = rms_box
        self.rms_box_bright = rms_box_bright
        self.threshold = threshold
        self.island = island
        self.group_tol = group_tol
        self.atrous_jmax = atrous_jmax
        self.shapelet_do = shapelet_do
        self.atrous_do = atrous_do
        self.beam = beam

    def find_sources(self, fits_image):
        """
        Find sources from CLEAN radio map

        :param fits_image: The input FITS image

        :return: The fitted sources
        """
        return bdsf.process_image(
            fits_image=fits_image,
            shapelet_do=self.shapelet_do,
            atrous_do=self.atrous_do,
            atrous_jmax=self.atrous_jmax,
            trim_box=self.trim_box,
            rms_box=self.rms_box,
            rms_box_bright=self.rms_box_bright,
            thresh_pix=self.threshold,
            thresh_isl=self.island,
            group_tol=self.group_tol,
            beam=self.beam,
        )


def main():
    parser = argparse.ArgumentParser(
        description="Source finds with PyBDSF on the pre and post peeling radio maps."
    )
    parser.add_argument(
        "--shapelet",
        action="store_true",
        help="Use the shapelet decomposition module. [default: False]",
        default=False,
    )
    parser.add_argument(
        "--atrous",
        action="store_true",
        help="Use the wavelet decomposition module. [default: False]",
        default=False,
    )
    parser.add_argument(
        "--jmax",
        help="Maximum allowed wavelength order for wavelet "
        + "decomposition. [default: 0 => calculate inside PyBDSF]",
        type=int,
        default=0,
    )
    parser.add_argument(
        "--threshold",
        help="Source detection threshold. [default: 5.0]",
        type=float,
        default=5.0,
    )
    parser.add_argument(
        "--island",
        help="Threshold for island boundary above the mean. "
        + "[default: 3.0]",
        type=float,
        default=3.0,
    )
    parser.add_argument(
        "--rms_box",
        help="Box size, step size for rms/mean map calculation. "
        + "Specify in pixels as box step. [default: None => "
        + "calculate inside PyBDSF]",
        type=float,
        nargs=2,
        default=None,
    )
    parser.add_argument(
        "--rms_box_bright",
        help="Box size, step size for rms/mean map calculation "
        + "near bright sources. Specify in pixels as box step. "
        + "[default: None => calculate inside PyBDSF]",
        type=float,
        nargs=2,
        default=None,
    )
    parser.add_argument(
        "--group_tol",
        help="Tolerance for grouping Gaussians into sources. "
        + "[default: 1]",
        type=float,
        default=1.0,
    )
    parser.add_argument(
        "--clobber",
        action="store_true",
        help="Resave pre and post peeling source catalogues. "
        + "[default: False]",
        default=False,
    )
    parser.add_argument(
        "--pre_file",
        help="Pre-peeling FITS map (contains the off-axis source).",
        type=str,
    )
    parser.add_argument(
        "--post_file",
        help="Post-peeling FITS map (off-axis source has been removed).",
        type=str,
    )

    args = parser.parse_args()

    begin = time.time()
    bmaj, bmin, bpa, naxis1, naxis2 = check_images(
        args.pre_file, args.post_file
    )

    # Get the source finding region
    xmin = 0.5 * (naxis1 - 0.33 * naxis1)
    ymin = 0.5 * (naxis2 - 0.33 * naxis2)
    xmax = xmin + 0.33 * naxis1
    ymax = ymin + 0.33 * naxis2
    trim_box = (xmin, xmax, ymin, ymax)

    if args.shapelet:
        print("   Shapelet decomposition enabled...")
    if args.atrous:
        print("   Wavelet decomposition enabled...")

    fitting_params = ProcessImage(
        args.trim_box,
        args.rms_box,
        args.rms_box_bright,
        args.threshold,
        args.island,
        args.group_tol,
        args.atrous_jmax,
        args.shapelet_do,
        args.atrous_do,
        beam=(bmaj, bmin, bpa),
    )

    print("\nSource finding on the pre-peeling map...")
    pre_fitted_img = fitting_params.find_sources(fits_image=args.pre_file)

    print("\nWriting pre-peeling source catalogues...")
    pre_fitted_img.write_catalog(
        outfile=os.path.join(os.path.dirname(args.pre_file), "_icomplist.bbs"),
        bbs_patches="source",
        catalog_type="gaul",
        clobber=args.clobber,
        format="bbs",
    )
    pre_fitted_img.write_catalog(
        outfile=os.path.join(os.path.dirname(args.pre_file), "_icomplist.csv"),
        catalog_type="srl",
        clobber=args.clobber,
        format="csv",
    )
    pre_fitted_img.write_catalog(
        outfile=os.path.join(
            os.path.dirname(args.pre_file), "_icomplist.ds9.reg"
        ),
        catalog_type="srl",
        clobber=args.clobber,
        format="ds9",
    )

    print("\nSource finding on the post-peeling map...")
    post_fitted_img = fitting_params.find_sources(fits_image=args.post_file)

    print("\nWriting post-peeling source catalogues...")
    post_fitted_img.write_catalog(
        outfile=os.path.join(
            os.path.dirname(args.post_file), "_icomplist.bbs"
        ),
        bbs_patches="source",
        catalog_type="gaul",
        clobber=args.clobber,
        format="bbs",
    )
    post_fitted_img.write_catalog(
        outfile=os.path.join(
            os.path.dirname(args.post_file), "_icomplist.csv"
        ),
        catalog_type="gaul",
        clobber=args.clobber,
        format="csv",
    )
    post_fitted_img.write_catalog(
        outfile=os.path.join(
            os.path.dirname(args.post_file), "_icomplist.ds9.reg"
        ),
        catalog_type="srl",
        clobber=args.clobber,
        format="ds9",
    )

    end = time.time()
    print(f"\nProcess finished in {datetime.timedelta(seconds=end - begin)}")


if __name__ == "__main__":
    main()
