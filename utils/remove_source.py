""" Calls the peeling routines and runs them on the maps """

from peel_tools import allotey, butler, hughes


class PeelTools:

    def __init__(
        self,
        pre_dir: str,
        src_dir: str,
        masked_model: list,
        solint: tuple[float, ...],
        uvrange: tuple[float, ...],
        all_stokes: bool = False,
        nov: bool = False,
    ):
        self.pre_dir = pre_dir
        self.src_dir = src_dir
        self.masked_model = masked_model
        self.solint = solint
        self.uvrange = uvrange
        self.all_stokes = all_stokes
        self.nov = nov

    def allotey(self, obs_ra, obs_dec):
        return allotey.run(
            self.pre_dir,
            self.src_dir,
            self.masked_model,
            self.solint,
            self.uvrange,
            obs_ra,
            obs_dec,
            self.all_stokes,
            self.nov,
        )

    def butler(self):
        return butler.run(
            self.pre_dir,
            self.src_dir,
            self.masked_model,
            self.solint,
            self.uvrange,
            self.all_stokes,
            self.nov,
        )

    def hughes(self):
        return hughes.run(
            self.pre_dir,
            self.src_dir,
            self.solint,
            self.uvrange,
            self.all_stokes,
            self.nov,
        )
