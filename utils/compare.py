"""Module for inspecting the effect of peeling on radio maps"""

import argparse
import datetime
import os
import time

import matplotlib.pyplot as plt
import numpy
import pandas
from astropy import units
from astropy.coordinates import SkyCoord


def _read_catalogue(
    source_catalogue: str,
) -> tuple[
    numpy.ndarray, numpy.ndarray, numpy.ndarray, numpy.ndarray, numpy.ndarray
]:
    """
    Reads source catalogue with pandas

    :param source_catalogue: Source catalogue in CSV format

    :return: The position of the science targets, their integrated
        flux densities and errors, and their peak flux densities
        and errors
    """
    df = pandas.read_csv(source_catalogue, low_memory=True)
    int_flux = df.Total_flux.to_numpy()
    int_flux_err = df.E_Total_flux.to_numpy()
    peak_flux = df.Peak_flux.to_numpy()
    peak_flux_err = df.E_Peak_flux.to_numpy()
    science_targets_positions = SkyCoord(
        ra=df.RA.to_numpy(), dec=df.DEC.to_numpy(), unit="deg"
    )

    return (
        science_targets_positions,
        int_flux,
        int_flux_err,
        peak_flux,
        peak_flux_err,
    )


def main():
    parser = argparse.ArgumentParser(
        description="Compare pre and post -peeling source catalogues for investigating "
        + "the impact of the peeling algorithm on the science targets in the maps."
    )
    parser.add_argument(
        "--pre_cat", help="Pre-peeling source catalogue.", type=str
    )
    parser.add_argument(
        "--post_cat", help="Post-peeling source catalogue.", type=str
    )
    parser.add_argument(
        "--sep",
        help="The maximum separation (in arcsec) between sources "
        + "for cross-matching pre and post -peeling source catalogues. "
        + "[default: 1.0]",
        type=float,
        default=1.0,
    )
    args = parser.parse_args()
    begin = time.time()

    max_sep = args.sep * units.arcsec
    print("Reading source parameters from pre-peeling catalogue...")
    (
        pre_science_targets_positions,
        pre_int_flux,
        pre_int_flux_err,
        pre_peak_flux,
        pre_peak_flux_err,
    ) = _read_catalogue(args.pre_cat)

    print("Reading source parameters from post-peeling catalogue...")
    (
        post_science_targets_positions,
        post_int_flux,
        post_int_flux_err,
        post_peak_flux,
        post_peak_flux_err,
    ) = _read_catalogue(args.post_cat)

    print(
        "Cross-matching the source catalogues with the separation constraint..."
    )
    idx, d2d, _ = pre_science_targets_positions.match_to_catalog_sky(
        post_science_targets_positions
    )
    sep_constraint = d2d < max_sep

    int_unpeel = post_int_flux[idx[sep_constraint]]
    int_unpeel_err = post_int_flux_err[idx[sep_constraint]]
    int_peel = pre_int_flux[sep_constraint]
    int_peel_err = pre_int_flux_err[sep_constraint]

    peak_unpeel = post_peak_flux[idx[sep_constraint]]
    peak_unpeel_err = post_peak_flux_err[idx[sep_constraint]]
    peak_peel = pre_peak_flux[sep_constraint]
    peak_peel_err = pre_peak_flux_err[sep_constraint]

    print("Computing the chi-square...")
    int_chi_sq = ((int_peel - int_unpeel) ** 2 / int_unpeel).sum()
    peak_chi_sq = ((peak_peel - peak_unpeel) ** 2 / peak_unpeel).sum()
    try:
        plt.figure(figsize=(14, 6), constrained_layout=True)
        plt.subplot(121)
        plt.plot(1e3 * int_unpeel, 1e3 * int_peel, "r.")
        plt.xlabel("Pre-peeling Integrated Flux(mJy)", fontsize=14)
        plt.ylabel("Post-peeling Integrated Flux (mJy)", fontsize=14)
        plt.title(f"$\\chi^{2}$ = {int_chi_sq:.4f}")

        plt.subplot(122)
        plt.plot(1e3 * peak_unpeel, 1e3 * peak_peel, "b.")
        plt.xlabel("Pre-peeling Peak Flux(mJy)", fontsize=14)
        plt.ylabel("Post-peeling Peak Flux (mJy)", fontsize=14)
        plt.title(f"$\\chi^{2}$ = {peak_chi_sq:.4f}")

        plt.savefig(
            os.path.join(os.path.dirname(args.post_cat), "inspect.png")
        )
        plt.show()
    except ValueError:
        print("\nNo source cross-match found!")

    end = time.time()
    print(f"\nProcess finished in {datetime.timedelta(seconds=end - begin)}")


if __name__ == "__main__":
    main()
