"""
Checks for MIRIAD visibility files and images and the directory
hosting the bright off-axis source visibilities
"""

import os


def check_peeling_data(
    input_dir: str,
    model_dir: str = None,
    all_stokes: bool = False,
    nov: bool = False,
):
    """
    Checks the input directory for the visibilities and model maps
    (based on user requested option(s)). These visibilities and model
    maps are required for a successful peeling. The CLEAN map is only
    required for the user to be able to create a deconvolution around
    the off-axis source to be removed.

    :param input_dir: Directory containing maps from which the off-axis
        source would be removed.
    :param all_stokes: Perform peeling on all Stokes (I, Q, U, V) maps.
    :param model_dir: Directory containing model map(s) to use.
    :param nov: Exclude peeling on Stokes V map.

    :return: None
    """
    print("Checking for visibilities and maps...")
    if not os.path.exists(f"{os.path.join(input_dir, 'visdata')}"):
        raise FileNotFoundError("MIRIAD visdata file missing!")
    if not os.path.exists(f"{os.path.join(input_dir, 'header')}"):
        raise FileNotFoundError("MIRIAD header file missing!")
    if not os.path.exists(f"{os.path.join(input_dir, 'history')}"):
        raise FileNotFoundError("MIRIAD history file missing!")
    if not os.path.exists(f"{os.path.join(input_dir, 'vartable')}"):
        raise FileNotFoundError("MIRIAD vartable file missing!")
    if not os.path.exists(f"{os.path.join(input_dir, 'flags')}"):
        raise FileNotFoundError("MIRIAD flags file missing!")

    # Check for model files
    # Stokes I
    if model_dir is None:
        model_dir = input_dir

    if not os.path.exists(os.path.join(model_dir, "_imodel")):
        raise FileNotFoundError(
            f"Cannot find {os.path.join(model_dir, '_imodel')}"
        )
    if not os.path.exists(f"{os.path.join(model_dir, '_irestor')}"):
        raise FileNotFoundError(
            "Cannot find %s" % os.path.join(model_dir, "_irestor")
        )
    if all_stokes:
        # Stokes Q
        if not os.path.exists(os.path.join(model_dir, "_qmodel")):
            raise FileNotFoundError(
                f"Cannot find {os.path.join(model_dir, '_qmodel')}"
            )
        # Stokes U
        if not os.path.exists(os.path.join(model_dir, "_umodel")):
            raise FileNotFoundError(
                f"Cannot find %s {os.path.join(model_dir, '_umodel')}"
            )
        if not nov:
            # Stokes V
            if not os.path.exists(os.path.join(model_dir, "_vmodel")):
                raise FileNotFoundError(
                    f"Cannot find {os.path.join(model_dir, '_vmodel')}"
                )


def get_off_axis_source_directory(off_axis_source_vis, uvrange, solint):
    """
    Get the directory containing the off-axis source visibilities.

    :param off_axis_source_vis: The visibilities containing only the
        off-axis source.
    :param uvrange: Sets of UVrange for self-calibration.
    :param solint: Solution interval (in mins) for self-calibration.

    :return: The directory containing the off-axis source visibilities.
    """
    if uvrange:
        if len(solint) == 1:
            off_axis_dir = os.path.join(
                off_axis_source_vis, "01_selfcal/0%s" % (len(uvrange))
            )
        else:
            off_axis_dir = os.path.join(
                off_axis_source_vis, "0%s_selfcal" % (len(solint))
            )
    else:
        off_axis_dir = os.path.join(
            off_axis_source_vis, "0%s_selfcal" % (len(solint))
        )

    return off_axis_dir
