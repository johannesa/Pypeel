""" Extracts the observation phase centre from the visibilities """

import os
import subprocess


def phase_centre(input_dir: str) -> tuple:
    """
    Extracts the observation phase centre from the visibilities.

    :param input_dir: Directory containing maps and visibilities
        from which the strong off-axis source would be removed.

    :return: The observation phase centre.
    """
    print("Extracting observation phase centre from the visibilities ...")
    if not os.path.exists(os.path.join(input_dir, "_uvlist.log")):
        subprocess.run(
            [
                "uvlist",
                f"vis={input_dir}",
                "options=source",
                f"log={os.path.join(input_dir, '_uvlist.log')}",
            ],
            check=True,
        )
    with open(
        os.path.join(input_dir, "_uvlist.log"), encoding="utf-8"
    ) as vis_logfile:
        lines = vis_logfile.readlines()
        vis_ra = (
            [line for line in lines if line.startswith("RA")][0]
            .replace(":", ",")
            .split(" ")[2]
        )
        vis_dec = (
            [line for line in lines if line.startswith("DEC")][0]
            .replace(":", ",")
            .split(" ")[-1]
        )

        return vis_ra, vis_dec
