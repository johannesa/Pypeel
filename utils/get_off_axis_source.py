""" """

import os
import re
import subprocess


def _create_region(pre_dir: str, _iclean: str):
    """
    Creates a region of interest on the CLEAN map around the off-axis source
    to be removed.

    :param pre_dir: Pre-peeling directory containing the visibilities, CLEAN
        map, and optionally the model maps.
    :param _iclean: CLEAN map containing the off-axis source.
    """
    # 1st round of creating the off-axis source region
    subprocess.run(
        [
            "cgcurs",
            "in=%s" % _iclean,
            "device=/xs",
            "labtyp=hms,dms",
            "range=-1e-3,1e-3",
            "options=region",
        ],
        check=True,
    )
    subprocess.run(
        ["mv", "cgcurs.region", "%s/cgcurs.region" % pre_dir], check=True
    )

    # 2nd round of creating the off-axis source region
    subprocess.run(
        [
            "cgcurs",
            "in=%s" % _iclean,
            "device=/xs",
            "labtyp=hms,dms",
            "options=region",
            "region=@%s/cgcurs.region" % pre_dir,
        ],
        check=True,
    )
    subprocess.run(["rm", "%s/cgcurs.region" % pre_dir], check=True)
    subprocess.run(
        ["mv", "cgcurs.region", "%s/cgcurs.region" % pre_dir], check=True
    )

    # 3rd round of creating the off-axis source region
    subprocess.run(
        [
            "cgcurs",
            "in=%s" % _iclean,
            "device=/xs",
            "labtyp=hms,dms",
            "options=region",
            "region=@%s/cgcurs.region" % pre_dir,
        ],
        check=True,
    )
    subprocess.run(["rm", "%s/cgcurs.region" % pre_dir], check=True)
    subprocess.run(
        ["mv", "cgcurs.region", "%s/cgcurs.region" % pre_dir], check=True
    )


def _get_off_axis_source_position(pre_dir, _iclean: str, region: str):
    """
    Fits a Gaussian to the manually created region of interest. This region
    contains only the off-axis source.

    :param pre_dir: Pre-peeling directory containing the visibilities, CLEAN
        map, and optionally the model maps.
    :param _iclean: CLEAN map containing the off-axis source.
    :param region: Deconvolution region around the off-axis source.

    :return: The Right Ascension and Declination of the off-axis source.
    """
    print("\nRunning IMFIT for the position of the off-axis source ...")
    bash_cmd = (
        "imfit in=%s object=gaussian "
        "region=%s"
        " | tee %s/_imfit.log" % (_iclean, region, pre_dir)
    )
    subprocess.run(bash_cmd, shell=True, check=True)
    flines = open("%s/_imfit.log" % pre_dir).readlines()
    source_ra = (
        [
            line
            for line in flines
            if re.search("Right Ascension", line, re.IGNORECASE)
        ][0]
        .split(" ")[-1]
        .replace(":", ",")
    )
    source_dec = (
        [
            line
            for line in flines
            if re.search("Declination", line, re.IGNORECASE)
        ][0]
        .split(" ")[-1]
        .replace(":", ",")
    )

    return source_ra, source_dec


def _mask_off_axis_source(
    models: str,
    region: str,
    all_stokes: bool = False,
    nov: bool = False,
):
    """
    Mask the off-axis source in the model map(s).

    :param models: Directory containing the model map(s).
    :param region: Deconvolution region around the off-axis source.
    :param all_stokes: Perform peeling on all Stokes (I, Q, U, V) maps.
    :param nov: Exclude peeling on Stokes V map.

    :return: None
    """
    print("\nMasking off-axis source in Stokes I model ...")
    subprocess.run(
        [
            "immask",
            "in=%s" % os.path.join(models, "_imodel"),
            "'region=%s'" % region,
            "flag=false",
            "logic=and",
        ],
        check=True,
    )
    if all_stokes:
        print("\nMasking off-axis source in Stokes Q model ...")
        subprocess.run(
            [
                "immask",
                "in=%s" % os.path.join(models, "_qmodel"),
                "'region=%s'" % region,
                "flag=false",
                "logic=and",
            ],
            check=True,
        )
        print("\nMasking off-axis source in Stokes U model ...")
        subprocess.run(
            [
                "immask",
                "in=%s" % os.path.join(models, "_umodel"),
                "'region=%s'" % region,
                "flag=false",
                "logic=and",
            ],
            check=True,
        )
        if not nov:
            print("\nMasking off-axis source in Stokes V model ...")
            subprocess.run(
                [
                    "immask",
                    "in=%s" % os.path.join(models, "_vmodel"),
                    "'region=%s'" % region,
                    "flag=false",
                    "logic=and",
                ],
                check=True,
            )


def _subtract_science_targets_with_gains_present(
    pre_dir: str,
    models: str,
    all_stokes: bool = False,
    nov: bool = False,
):
    """
    Subtract the science targets from the visibilities when gain solutions
    already exist. The remaining UV data is that of the off-axis source.

    :param pre_dir: Pre-peeling directory containing the visibilities, CLEAN
        map, and optionally the model maps.
    :param models: Directory containing the model map(s).
    :param all_stokes: Perform peeling on all Stokes (I, Q, U, V) maps.
    :param nov: Exclude peeling on Stokes V map.

    :return: None
    """
    print("\nSubtracting I science targets from the UV data ...")
    subprocess.run(
        [
            "uvmodel",
            "vis=%s/01_uvaver" % pre_dir,
            "model=%s" % os.path.join(models, "_imodel"),
            "out=%s/02_uvmodel_I" % pre_dir,
            "options=subtract,mfs",
        ],
        check=True,
    )
    if all_stokes:
        print(
            "\nSubtracting Q science targets from the UV "
            + "Stokes-I subtracted data ..."
        )
        subprocess.run(
            [
                "uvmodel",
                "vis=%s/02_uvmodel_I" % pre_dir,
                "model=%s" % os.path.join(models, "_qmodel"),
                "out=%s/02_uvmodel_Q" % pre_dir,
                "options=subtract,mfs",
            ],
            check=True,
        )
        print(
            "\nSubtracting U science targets from the UV "
            + "Stokes-Q subtracted data ..."
        )
        subprocess.run(
            [
                "uvmodel",
                "vis=%s/02_uvmodel_Q" % pre_dir,
                "model=%s" % os.path.join(models, "_umodel"),
                "out=%s/02_uvmodel_U" % pre_dir,
                "options=subtract,mfs",
            ],
            check=True,
        )
        subprocess.run(
            [
                "rm",
                "-r",
                "%s/02_uvmodel_I" % pre_dir,
                "%s/02_uvmodel_Q" % pre_dir,
            ],
            check=True,
        )
        if nov:
            subprocess.run(
                [
                    "mv",
                    "%s/02_uvmodel_U" % pre_dir,
                    "%s/02_uvmodel" % pre_dir,
                ],
                check=True,
            )
        else:
            print(
                "\nSubtracting V science targets from the UV "
                + "Stokes-U subtracted data ..."
            )
            subprocess.run(
                [
                    "uvmodel",
                    "vis=%s/02_uvmodel_U" % pre_dir,
                    "model=%s" % os.path.join(models, "_vmodel"),
                    "out=%s/02_uvmodel_V" % pre_dir,
                    "options=subtract,mfs",
                ],
                check=True,
            )
            subprocess.run(
                ["rm", "-r", "%s/02_uvmodel_U" % pre_dir], check=True
            )
            subprocess.run(
                [
                    "mv",
                    "%s/02_uvmodel_V" % pre_dir,
                    "%s/02_uvmodel" % pre_dir,
                ],
                check=True,
            )
    else:
        subprocess.run(
            [
                "mv",
                "%s/02_uvmodel_I" % pre_dir,
                "%s/02_uvmodel" % pre_dir,
            ],
            check=True,
        )


def _subtract_science_targets_with_no_gains_present(
    pre_dir, models, all_stokes, nov
):
    """
    Subtracts the science targets from the visibilities when there are no
    existing gain solutions. The remaining UV data is that of the off-axis
    source

    :param pre_dir: Pre-peeling directory containing the visibilities, CLEAN
        map, and optionally the model maps.
    :param models: Directory containing the model map(s).
    :param all_stokes: Perform peeling on all Stokes (I, Q, U, V) maps.
    :param nov: Exclude peeling on Stokes V map.

    :return: None
    """
    print("\nSubtracting I science targets from the UV data ...")
    subprocess.run(
        [
            "uvmodel",
            "vis=%s" % pre_dir,
            "model=%s" % os.path.join(models, "_imodel"),
            "out=%s/01_uvmodel_I" % pre_dir,
            "options=subtract,mfs",
        ],
        check=True,
    )
    if all_stokes:
        print(
            "\nSubtracting Q science targets from the UV "
            + "Stokes-I subtracted data ..."
        )
        subprocess.run(
            [
                "uvmodel",
                "vis=%s/01_uvmodel_I" % pre_dir,
                "model=%s" % os.path.join(models, "_qmodel"),
                "out=%s/01_uvmodel_Q" % pre_dir,
                "options=subtract,mfs",
            ],
            check=True,
        )
        print(
            "\nSubtracting U science targets from the UV "
            + "Stokes-Q subtracted data ..."
        )
        subprocess.run(
            [
                "uvmodel",
                "vis=%s/01_uvmodel_Q" % pre_dir,
                "model=%s" % os.path.join(models, "_umodel"),
                "out=%s/01_uvmodel_U" % pre_dir,
                "options=subtract,mfs",
            ],
            check=True,
        )
        subprocess.run(
            [
                "rm",
                "-r",
                "%s/01_uvmodel_I" % pre_dir,
                "%s/01_uvmodel_Q" % pre_dir,
            ],
            check=True,
        )
        if nov:
            subprocess.run(
                [
                    "mv",
                    "%s/01_uvmodel_U" % pre_dir,
                    "%s/01_uvmodel" % pre_dir,
                ],
                check=True,
            )
        else:
            print(
                "\nSubtracting V science targets from the UV "
                + "Stokes-U subtracted data ..."
            )
            subprocess.run(
                [
                    "uvmodel",
                    "vis=%s/01_uvmodel_U" % pre_dir,
                    "model=%s" % os.path.join(models, "_vmodel"),
                    "out=%s/01_uvmodel_V" % pre_dir,
                    "options=subtract,mfs",
                ],
                check=True,
            )
            subprocess.run(
                [
                    "mv",
                    "%s/01_uvmodel_V" % pre_dir,
                    "%s/01_uvmodel" % pre_dir,
                ],
                check=True,
            )
            subprocess.run(
                ["rm", "-r", "%s/01_uvmodel_U" % pre_dir], check=True
            )
    else:
        subprocess.run(
            [
                "mv",
                "%s/01_uvmodel_I" % pre_dir,
                "%s/01_uvmodel" % pre_dir,
            ],
            check=True,
        )


def src_position(
    pre_dir: str, model_dir: str, all_stokes: bool = False, nov: bool = False
):
    """
    Fits for the position of the off-axis source and masks it
    out in the model map.

    :param pre_dir: Directory containing map from which off-axis would be removed.
    :param model_dir: Directory containing model map(s) to use.
    :param all_stokes: Perform peeling on all Stokes (I, Q, U, V) maps.
    :param nov: Exclude peeling on Stokes V map.

    :return: Visibilities containing the off-axis source at the phase centre
        and the model containing the science target(s).
    """
    if model_dir is None:
        models = pre_dir
    else:
        models = model_dir

    if not os.path.isfile("%s/cgcurs.region" % pre_dir):
        # Create region around off-axis source with MIRIAD's PGPLOT if
        # the region file does not exist
        _create_region(pre_dir, os.path.join(models, "_irestor"))
    region = os.path.join("@%s" % pre_dir, "cgcurs.region")

    if os.path.isfile("%s/gains" % models):
        # Apply calibration solutions
        if not os.path.isdir("%s/01_uvaver" % pre_dir):
            print("\nApplying gain solutions to the original UV data...")
            subprocess.run(
                [
                    "uvaver",
                    "vis=%s" % pre_dir,
                    "stokes=i,q,u,v",
                    "out=%s/01_uvaver" % pre_dir,
                ],
                check=True,
            )

        # Mask the off-axis source in the model map(s)
        if not os.path.isdir("%s/02_uvmodel" % pre_dir):
            _mask_off_axis_source(models, region, all_stokes, nov)

            # Subtract the science targets from the visibilities
            # Remaining UV data is that of the off-axis source
            _subtract_science_targets_with_gains_present(
                pre_dir, models, all_stokes, nov
            )

        if not os.path.isdir("%s/03_uvedit/" % pre_dir):
            print("\nFitting for the position of the off-axis source...")
            off_axis_source_ra, off_axis_source_dec = (
                _get_off_axis_source_position(
                    pre_dir, os.path.join(models, "_irestor"), region
                )
            )

            print(
                "\nShifting UV data towards the direction of "
                + "the off-axis source..."
            )
            subprocess.run(
                [
                    "uvedit",
                    "vis=%s" % os.path.join(pre_dir, "02_uvmodel"),
                    "ra=%s" % off_axis_source_ra,
                    "dec=%s" % off_axis_source_dec,
                    "out=%s" % os.path.join(pre_dir, "03_uvedit"),
                ],
                check=True,
            )
            subprocess.run(
                ["mv", "%s/_imfit.log" % pre_dir, "%s/03_uvedit" % pre_dir],
                check=True,
            )
        off_axis_source_dir = os.path.join("%s" % pre_dir, "03_uvedit")

    else:
        # Mask the off-axis source in the model map(s)
        if not os.path.isdir("%s/01_uvmodel" % pre_dir):
            _mask_off_axis_source(models, region, all_stokes, nov)

            #  Subtract the science targets from the visibilities
            # Remaining UV data is that of the confusing source
            _subtract_science_targets_with_no_gains_present(
                pre_dir, models, all_stokes, nov
            )

        if not os.path.isdir("%s/02_uvedit/" % pre_dir):
            print("\nFitting for the position of the off-axis source...")
            off_axis_source_ra, off_axis_source_dec = (
                _get_off_axis_source_position(
                    pre_dir, os.path.join(models, "_irestor"), region
                )
            )

            print(
                "\nShifting UV data towards the direction of the off-axis source..."
            )
            subprocess.run(
                [
                    "uvedit",
                    "vis=%s" % os.path.join(pre_dir, "01_uvmodel"),
                    "ra=%s" % off_axis_source_ra,
                    "dec=%s" % off_axis_source_dec,
                    "out=%s" % os.path.join(pre_dir, "02_uvedit"),
                ],
                check=True,
            )
            subprocess.run(
                ["mv", "%s/_imfit.log" % pre_dir, "%s/02_uvedit" % pre_dir],
                check=True,
            )
        off_axis_source_dir = os.path.join("%s" % pre_dir, "02_uvedit")

    if all_stokes:
        if nov:
            output = off_axis_source_dir, (
                os.path.join(models, "_imodel"),
                os.path.join(models, "_qmodel"),
                os.path.join(models, "_umodel"),
            )
        else:
            output = off_axis_source_dir, (
                os.path.join(models, "_imodel"),
                os.path.join(models, "_qmodel"),
                os.path.join(models, "_umodel"),
                os.path.join(models, "_vmodel"),
            )
    else:
        output = off_axis_source_dir, os.path.join(models, "_imodel")

    return output
